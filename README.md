# README #

tao_web started as an example for tao_core but eventually became its own library...

### Outline ###

* http and http_* various different http libraries
* web_widget.py - a full on widget framework for complex css and javascript pages
* websocket.py - a replacement http server channel implementation which supports websocket, primarily for use with WebsocketView in web_widget

### Installation ###

the library is written for python2 but supports python 3 to create a python3 version of the libraray...

- run 2to3 on library converting in place
- copy the files in the py3 directory over the resulting source

### Contribution guidelines ###

contributions welcome of course.

### Who do I talk to? ###

