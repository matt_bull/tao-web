from tao_web.http import HttpServerProtocol
from tao_web.http_client import ClientDispatcher
#from tao_core.tdf_core import MappingDispatcher,TrackDispatcher,Dispatcher,dispatchWithCallback,SimpleEvent
from tao_core import sigIO
from tao_core.sigIO_net import TCPInetTransport

class ProxyClientDispatcher(ClientDispatcher):
	# TODO: For completeness this should really catch https dispatch as well just in case
	def finish(self,event,exc=None):
		if event['resp_code'] in (301,302,307):
			# redirect.... check we are not redirecting to https
			sch,host,rpath,para,query,frag=urllib.parse.urlparse(event['resp_headers']['location'],scheme='http')
			if scheme=='https':
				# nope don't do it return to client and let them deal with it
				TrackDispatcher.finish(self,event,exc=exc)
				return
		# OK everything else handle as normal....
		ClientDispatcher.finish(self,event,exxc=exc)

class HttpProxyLog(TrackDispatcher):
	def __init__(self,dispatcher):
		self._dispatcher=dispatcher
		TrackDispatcher.__init__(self)
	def dispatch(self,evt,cb=None):
		print "REQUEST-",evt['hostname'],evt['req_path'],evt['req_query']
		return TrackDispatcher.dispatch(self,evt,cb=cb)
	def finish(self,evt,exc=None):
		print "RESPONSE-",evt['req_path'],evt['req_query'],evt['resp_code']
		for it in evt['resp_headers'].items():
			print "\t%s:%s" % it
		TrackDispatcher.finish(self,evt,exc=exc)
		print "Pending->"
		for evt in self.cb_map.keys():
			print '>>',evt['hostname'],evt['req_path']
 
if __name__=='__main__':
	import time,os
	io=sigIO.getIO()
	io.start()
	c_d=ProxyClientDispatcher()
	log=HttpProxyLog(c_d)
	s_ch=io.newChannel(HttpServerProtocol,TCPInetTransport,dispatcher=log)
	s_ch.listen((os.getenv('IP','0.0.0.0'),int(os.getenv('PORT',8080))))
	while 1:
		try:
			time.sleep(90)
		except KeyboardInterrupt:
			break
	s_ch.close()
	c_d.close()
	io.stop()

