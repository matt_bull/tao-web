from tao_web.web_widget import View,ViewWrapper
from tao_core.tdf_core import TrackDispatcher,dispatchWithCallback
import urllib.parse,hashlib,base64,types
from tao_core.JEP import JEPProtocol
from tao_web import websocket
import codecs

class WebSocketView(View):
	_embed_paths=['websocket.js','alert.css']
	_js_init="setupData('%(doc_uri)s','%(proto)s');"
	_proto_name_='echo'
	def getInitArgs(self,doc_uri=None):
		args=View.getInitArgs(self,doc_uri=None)
		args['proto']=self._proto_name_
		return args
	def getWebsocketDispatcher(self):
		# override to return dispatcher for websocket connection subprotocol
		#raise NotImplemented("getWebsocketDispatcher not implemented in WebSocketView")
		return self._dispatcher
	def dispatch(self,event,cb=None):
		print("WebSocketView ->",event['req_headers'],self._proto_name_)
		#if event['req_headers'].has_key('upgrade'):
		#	#print "WebSocketView ->",event['req_headers']['upgrade'].lower(),event['req_headers']['sec-websocket-protocol'],self._proto_name_
		if 'upgrade' in event['req_headers'] and event['req_headers']['upgrade'].lower()=='websocket':
			# add in websocket dispatcher here, so downstream dispatcher can register it if the want to....
			event['dispatcher']=self.getWebsocketDispatcher()
			if self._dispatcher:
				if 'validated' in event and not event['validated'] and not event['query']:
					# we have a data dispatcher which may be doing auth and we have not been validated upstream...
					# set query and dispatch for authentication....		
					event['query']=('_user_auth_',[],[])
			print("diverting to TrackDispatcher....",event,cb)
			return TrackDispatcher.dispatch(self,event,cb=cb)
			#return dispatchWithCallback(self.d_s,event,self.finish)
		elif cb:
			return super(WebSocketView, self).dispatch(event,cb=cb)
			#return dispatchWithCallback(self.view,event,self.finish)
	def finish(self,event,exc=None):
		#print("WebSocketView.finish",event['req_headers'])
		if 'upgrade' in event['req_headers'] and event['req_headers']['upgrade'].lower()=='websocket' and event['req_headers']['sec-websocket-protocol']==self._proto_name_:
			#print("WebSocketView.finish", end=' ')
			if exc or ('validated' in event and not event['validated']):
				event['resp_code']=401
			event['resp_code']=101
			event['resp_headers']={'Upgrade':'WebSocket','Connection':'Upgrade','Sec-WebSocket-Protocol':self._proto_name_}
			key=event['req_headers']['sec-websocket-key']+'258EAFA5-E914-47DA-95CA-C5AB0DC85B11'
			key=codecs.encode(key,'UTF-8')
			key=hashlib.sha1(key)
			key=base64.b64encode(key.digest())
			event['resp_headers']['Sec-WebSocket-Accept']=codecs.decode(key,'UTF-8')
			TrackDispatcher.finish(self,event,exc=exc)
		else:
			View.finish(self,event,exc=exc)
	def render(self,data,doc_uri=None):
		# override to output html around body.
		#print body
		page=View.render(self,data,doc_uri)
		#print type(page)
		if type(page)==str:
			yield "<span id='websocket_uri' style='display:none;'>%s</span><div class='alert' id='ws_conn_state'>(Not Connected)</span>%s" % (self.doc_uri,page)
		elif type(page)==types.GeneratorType:
			yield "<span id='websocket_uri' style='display:none;'>%s</span><div class='alert' id='ws_conn_state'>(Not Connected)</span>" % self.doc_uri
			for chnk in page:
				#print 'CHUNK ->',chnk
				yield chnk
		else:
			page.insert(0,"<span id='websocket_uri' style='display:none;'>%s</span><div class='alert' id='ws_conn_state'>(Not Connected)</span>" % (self.doc_uri))
			yield page

websocket.registerProtocol('JEP',JEPProtocol)

class JepWSView(WebSocketView):
	_embed_paths=['websocket.js','jep.js']
	_js_init="setupJEP('%(doc_uri)s');"
	_proto_name_='JEP'

class WebSocketWrapper(ViewWrapper):
	_embed_paths=['websocket.js']
	_js_init="setupData('%(doc_uri)s','%(proto)s');"
	_proto_name_='echo'
	def getWebsocketDispatcher(self):
		# override to return dispatcher for websocket connection subprotocol
		#raise NotImplemented("getWebsocketDispatcher not implemented in WebSocketView")
		return self
	def dispatch(self,event,cb=None):
		#print "WebSocketWrapper ->",event.name
		if 'upgrade' in event['req_headers'] and event['req_headers']['upgrade']=='websocket' and event['req_headers']['sec-websocket-protocol']==self._proto_name_:
			event['resp_code']=101
			event['resp_headers']={'Upgrade':'WebSocket','Connection':'Upgrade','Sec-WebSocket-Protocol':self._proto_name_}
			key=event['req_headers']['sec-websocket-key']+'258EAFA5-E914-47DA-95CA-C5AB0DC85B11'
			try:
				key=codecs.encode(event['req_headers']['sec-websocket-key']+'258EAFA5-E914-47DA-95CA-C5AB0DC85B11','UTF-8')
				key=hashlib.sha1(key)
				key=base64.b64encode(key.digest())
				event['resp_headers']['Sec-WebSocket-Accept']=codecs.decode(key,'UTF-8')
			except Exception as exc:
				print('ERR>>>>',exc)
			event['dispatcher']=self.getWebsocketDispatcher()
		elif cb:
			return super(WebSocketWrapper, self).dispatch(event,cb=cb)
			#return dispatchWithCallback(self.view,event,self.finish)
	def render(self,data,body=''):
		widget="<span id='websocket_uri' style='display:none;'>%s</span><span id='ws_conn_state'>(Not Connected)</span>" % self.doc_uri
		if type(body)==str:
			body=widget+body
		else:
			body.insert(0,widget)
		return body

	

if __name__=="__main__":
	from tao_core import sigIO,sigIO_net
	from tao_core.tdf_core import SimpleEvent
	from tao_core.utils import CompositeException
	from tao_web import websocket
	from tao_web.tests.utils import testDataSource
	import time,json,os
	io_main=sigIO.getIO()
	io_main.start()
	class EchoProtocol(sigIO.Protocol):
		__in_buffer_class__=sigIO.SimpleBuffer
		def onData(self):
			for packet in self.in_buffer:
				self.put(SimpleEvent('org.digitao.echo',**json.loads(packet)),sync=True)
		def onCallback(self,evt,exc=None):
			if exc:
				if isinstance(exc,CompositeException):
					resp=['ERROR',{'exception':str(exc),'tb':exc.stack}]
				else:
					resp=['ERROR',{'exception':repr(exc),'tb':[]}]
			else:
				resp=['OK',evt.getDict()]
			self.send(json.dumps(resp))
	websocket.registerProtocol('echo', EchoProtocol)
	class TestWSView(WebSocketView):
		def getWebsocketDispatcher(self):
			return self.echoDispatch
		def echoDispatch(self,evt,cb=None):
			print(evt.getDict())
			evt['echo_resp']=evt['data']
	view=TestWSView(testDataSource,doc_uri='http://localhost:8081/test')
	#view=WebSocketView(TestView('http://localhost:8080/test'))
	chan=io_main.newChannel(websocket.HttpWSProtocol,sigIO_net.TCPInetTransport,dispatcher=view,trans_args={'allow_reuse_addr':True})
	chan.listen((os.getenv('IP','0.0.0.0'),int(os.getenv('PORT',8081))))
	while 1:
		try:
			time.sleep(20)
		except KeyboardInterrupt:
			break
	chan.close()
	io_main.stop()
