from setuptools import setup

setup(name='tao_web',
      version='0.6',
      description='Tao Web framwork',
      author='Matt Bull',
      author_email='matt@digitao.org',
      url='https://digitao.org',
      packages=['tao_web'],
      requires=['tao_core (>=0.6)']
     )
