## simplest possible Http server for sigIO ##
import time,copy
import sys,urlparse,types,socket,urllib,traceback
import threading
import thread,json
from cStringIO import StringIO
from __builtin__ import int
from Queue import Empty as QEmpty
#this is just for example
# if you want to use this in anger remove this and prepend
# tao_core. to the imports below....
#sys.path.append('../')

# we don't really use tdf_core here but the event class (with defaults)
# makes things simpler...
from tao_core.tdf_core import SimpleEvent,QueueDispatcher,DispatchException
from tao_core import sigIO

ST_IDLE=1 # idle expect request / response line
ST_HEADER=2 # expect header line
ST_BODY=3 # expect body section...
ST_CBODY=4 # expect chunked body.....
ST_CLOSE=5 # when all data sent close connection...
ST_TUNNEL=6 #only used by proxy to tunnel ssl

def_mime_type='text/plain'
error_pages={
	404:'<html><head><title>404 - Document Not Found</title></head><body><font family=arial,helvetica><h2>404 - Document Not Found</h2>The requested document %s was not found</font></body></html>',
	500:'<html><head><title>500 - Internal Server Error</title></head><body><font family=arial,helvetica><h2>500 - Internal Server Error</h2>The request for %s could not be served due to an internal server error</font></body></html>'
	}
resp_message={
	200:'OK',
	201:'Created',
	202:'Accepted',
	203:'Partial Information',
	204:'No Response',
	301:'Moved',
	302:'Found',
	303:'Method',
	304:'Not Modified',
	400:'Bad Request',
	401:'Unauthorized',
	402:'Payment Required',
	403:'Forbidden',
	404:'Not Found',
	500:'Internal Error',
	501:'Not Implemented',
	502:'Service Temporarily Overloaded',
	503:'Gateway Timeout',
	505:'HTTP Version Not Supported'
	}
weekdayname = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
monthname = [None,'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun','Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

def httpDate(offset=0,dat=None):
	"""Return the current date and time formatted for a message header."""
	if not dat:
		dat = time.time()+offset
	year, month, day, hh, mm, ss, wd, y, z = time.gmtime(dat)
	s = "%s, %02d %3s %4d %02d:%02d:%02d GMT" % (
			weekdayname[wd],
			day, monthname[month], year,
			hh, mm, ss)
	return s

def httpMatchPath(evt,path):
	if path=='':
		#everything matches an empty string
		return True
	#print ">> path match",path,evt['disp_path']
	path=path.strip('/').split('/')
	#print ">> path match",path,evt['disp_path']
	if evt['disp_path'][:len(path)]==path:
		evt['disp_path']=evt['disp_path'][len(path):]
		return True
	else:
		return False

def httpGetUri(evt,**kwargs):
	if kwargs:
		evt=evt.getDict()
		evt.update(kwargs)
	if (evt['scheme']=='http' and evt['port']==80) or (evt['scheme']=='https' and evt['port']==443):
		return "%s://%s%s" % (evt['scheme'],evt['hostname'],evt['req_path'])
	else:
		return "%s://%s:%s%s" % (evt['scheme'],evt['hostname'],evt['port'],evt['req_path'])

def chunkedProducer(body,chunk_size=1024):
	curr_chunk=''
	for chunk in body:
		#print type(chunk),chunk
		if type(chunk) in (str,unicode):
			curr_chunk=curr_chunk+chunk
			if len(curr_chunk)>chunk_size:
				yield '%s\r\n%s\r\n' % (hex(len(curr_chunk))[2:],curr_chunk)
				curr_chunk=''
		else:
			if curr_chunk:
				# send curr_chunk before recursing
				yield '%s\r\n%s\r\n' % (hex(len(curr_chunk))[2:],curr_chunk)
				curr_chunk=''
			for p in chunkedProducer(chunk):
				yield p
	if curr_chunk:
		yield '%s\r\n%s\r\n' % (hex(len(curr_chunk))[2:],curr_chunk)

def fileProducer(fpath,c_size=4096):
	f=open(fpath,'rb')
	while 1:
		dat=f.read(c_size)
		yield dat
		if len(dat)<c_size:
			break

def parseAuthHeader(h_str):
	r={}
	for pair in h_str.split(','):
		k,v=pair.strip().split('=',1)
		if v[0]=='"' and v[-1]=='"':
			v=v.strip('"')
		r[k]=v
	return r

def parseHeader(h_str):
	r={}
	for pair in h_str.split(';'):
		k,v=pair.lower().strip().split('=',1)
		if v[0]=='"' and v[-1]=='"':
			v=v.strip('"')
		r[k]=v
	return r

def parseCookie(h_str):
	r={}
	for pair in h_str.split(';')[1:]:
		try:
			k,v=pair.lower().strip().split('=',1)
			if v and v[0]=='"' and v[-1]=='"':
				v=v.strip('"')
		except:
			k=pair.lower().strip()
			v=None
		r[k]=v
	return (h_str.split(';')[0],r)

class MimeFile(object):
	def __init__(self,mtype,fname):
		self.mime_type=mtype
		self.name=fname
		if mtype.startswith('text'):
			self.value=''
		else:
			self.file=StringIO()
	def write(self,chunk):
		if self.mime_type.startswith('text') and type(chunk) in (str,unicode):
			self.value=self.value+chunk
		else:
			self.file.write(chunk)
	def seek(self,offset):
		if hasattr(self,'file'):
			self.file.seek(offset)
	def read(self,flen=None):
		if hasattr(self,'value'):
			return self.value
		elif flen:
			return self.file.read(flen)
		else:
			self.file.seek(0)
			return self.file.read()

def parseMultipart(body,bound):
	data={}
	value=False
	for line in body.split('\r\n'):
		#print line
		if line=='--'+bound:
			disp=None
			prm={'Content-Type':'text/plain'}
			value=False
		elif line=='--'+bound+'--':
			# end of
			#print "done - end of multipart"
			return data
		elif line.startswith('Content-Disposition'):
			# first line of a section
			prm_str=line.split(':')[1].split(';',1)[1]
			prm.update(parseHeader(prm_str))
		elif value:
			if prm['Content-Type'].startswith('application/json'):
				data[prm['name']]=json.loads(line)
			elif prm.has_key('filename'):
				if not data.has_key(prm['name']):
					data[prm['name']]=MimeFile(prm['Content-Type'],prm['filename'])
				data[prm['name']].write(line+'\r\n')
			else:
				data[prm['name']]=line
		elif ':' in line:
			#print line
			k,v=line.split(':')
			prm[k.strip()]=v.strip()
		elif line=='':
			# end of headers...
			value=True

class HttpRequest(SimpleEvent):
	"""event representing a single http request"""
	defaults={
			'req_path':'/index.htm',
			'req_type':'GET',
			'req_version':'1.2',
			'req_headers':{},
			'req_query':{},
			'hostname':'',
			'port':80,
			'req_body':None,
			'http1':True,
			'resp_code':0, # so we can set this sensibly and track if a renderer has already set it
			'resp_headers':{'x-clacks-overhead':'GNU Terry Pratchett'}, # RIP
			#'resp_message':'File Not Found',
			'resp_body':None,
			'resp_type':'',
			'doctypes':[],
			}
	def __init__(self,req_type,req_uri,req_version='1.2',headers={},body="",**data):
		data.update({'req_type':req_type,'req_version':req_version,'req_headers':headers})
		#tweak a couple of things on the way in
		sch,host,data['req_path'],para,query,frag=urlparse.urlparse(req_uri,scheme='http')
		data['scheme']=sch
		if sch.startswith('http'):
			if ':' in host:
				data['hostname'],data['port']=host.split(':',1)
				data['port']=int(data['port'])
			else:
				data['hostname']=host
		elif headers.has_key('Host'):
			data['hostname']=headers['Host']
		elif not sch.startswith('http'):
			raise TypeError('wrong url scheme for HttpRequest - %s' % sch)
		if query:
			data['req_query']=urlparse.parse_qs(query)
		if data['scheme']=='https':
			if not data.has_key('port'):
				data['port']=443
		if req_version.split('.')[0]=='0':
			data['http1']=False
		else:
			data['req_body']=body
		# strip leading slashes and split ready for matching...
		data['disp_path']=data['req_path'].lstrip('/').split('/')
		SimpleEvent.__init__(self,"org.digitao.tao_web.HttpRequest",**data)
	# refactored to an external function
	# linked here for comapatibility, remove in future
	def matchPath(self,path):
		return httpMatchPath(self,path)
	# refactored to an external function
	# linked here for comapatibility, remove in future
	def getUri(self):
		return httpGetUri(self)

class HttpProtocol(sigIO.Protocol):
	"""HTTP protocol class"""
	def __pinit__(self,**kwargs):
		sigIO.Protocol.__pinit__(self)
		self.in_buffer=sigIO.LateBoundBuffer(sep='\r\n')
		self.producer=sigIO.FifoProducer()
		self.pstate=ST_IDLE
		self.curr_headers={}
		self.curr_request=None
		self.body_part=''
	def onData(self):
		"""Generate HTTP request events from in_buff"""
		#print 'onData'
		while 1:
			if self.pstate==ST_IDLE:
				#print "new request ->",self.in_buffer.peek()
				try:
					self.curr_request=self.in_buffer.next().split(' ',2)
					# ensure we aren't catching an empty line from a previous request...
					if len(self.curr_request)==3:
						self.pstate=ST_HEADER
				except StopIteration:
					break
				#except:
				#	self.close()
			elif self.pstate==ST_HEADER:
				#print "HEADER",self.curr_request
				try:
					req_line=self.in_buffer.next().strip()
				except StopIteration:
					break
				#print "header->",req_line
				if req_line=="":
					self.endHeaders()
				else:
					key,val=req_line.split(':',1)
					key=key.strip().lower()
					if self.curr_headers.has_key(key):
						if isinstance(self.curr_headers[key],str):
							self.curr_headers[key]=[self.curr_headers[key]]
						self.curr_headers[key].append(val.strip())
					else:
						self.curr_headers[key]=val.strip()
			elif self.pstate==ST_BODY:
				#print "BODY"
				if self.curr_headers.has_key('content-length') and len(self.body_part)==int(self.curr_headers['content-length']):
					#print "end of lengthed body"
					self.onPacket(self.curr_request,head=self.curr_headers,body=self.body_part)
					self.curr_request=None
					self.curr_headers={}
					self.body_part=''
					self.pstate=ST_IDLE
				elif not self.curr_headers.has_key('content-length'):
					#print self.in_buffer
					for b_line in self.in_buffer:
						if len(b_line)>0:
							self.body_part=self.body_part+b_line
						else:
							self.onPacket(self.curr_request,head=self.curr_headers,body=self.body_part)
							self.curr_request=None
							self.curr_headers={}
							self.body_part=''
							self.pstate=ST_IDLE
				try:
					if self.curr_headers.has_key('content-length'):
						self.body_part=self.body_part+self.in_buffer.next(plen=int(self.curr_headers['content-length'])-len(self.body_part))
					else:
						self.body_part=self.body_part+self.in_buffer.next()
				except StopIteration:
					#print 'Spybreak! ->',len(self.body_part)
					break
				#print len(self.body_part)
			elif self.pstate==ST_CBODY:
				#print "CBODY"
				if self.c_len:
					# read chunk and update self.c_len
					try:
						http_chunk=self.in_buffer.next(plen=self.c_len)
					except StopIteration:
						break
					self.c_len=self.c_len-len(http_chunk)
					self.body_part=self.body_part+http_chunk
					try:
						self.in_buffer.next(plen=2) # trailer
					except StopIteration:
						pass
				else:
					# get the chunk length....
					try:
						#self.c_len=int(self.in_buffer.next(),16)
						h_len=self.in_buffer.next()
					except StopIteration:
						break
					try:
						self.c_len=int(h_len,16)
					except ValueError:
						#self.c_len=0
						print 'incorrect value for length',h_len
					if self.c_len==0:
						# zero length chunk..... done
						#print "end of chunked body"
						self.onPacket(self.curr_request,head=self.curr_headers,body=self.body_part)
						self.curr_request=None
						self.curr_headers={}
						self.body_part=''
						self.pstate=ST_IDLE
						# deal with the trailing CRLF
						try:
							self.in_buffer.next()
						except StopIteration:
							pass
			else:
				# unknown pstate... bail
				break
	def endHeaders(self):
		#print ">>>>> end headers"
		if self.curr_headers.has_key('content-length') and self.curr_headers['content-length']!='0':
			# end of headers and expecting body
			self.pstate=ST_BODY
		elif self.curr_headers.has_key('connection') and self.curr_headers['connection']=='close':
			self.pstate=ST_BODY
		elif self.curr_headers.has_key('transfer-encoding'):
			if "chunked" in self.curr_headers['transfer-encoding']:
				# chunked encoding....
				self.pstate=ST_CBODY
				self.c_len=0
		else:
			# end of headers.. and no body...
			#print "headers only ->",self.curr_headers
			self.onPacket(self.curr_request,head=self.curr_headers)
			self.curr_request=None
			self.curr_headers={}
			self.pstate=ST_IDLE
	def onPacket(self,req,head={},body=''):
		"""depends on if we are client or server"""
		pass

class HttpClientDispatcher(HttpProtocol,QueueDispatcher):
	def __pinit__(self,**kwargs):
		self.response_wait=thread.allocate_lock()
		self.req_event=None
		self.auth_cache=None
		QueueDispatcher.__init__(self)
		HttpProtocol.__pinit__(self,**kwargs)
	def dispatch(self,evt,cb=None):
		# we should really check the event is for the correct host here...
		if self.state==sigIO.ST_CLOSED and getattr(self,'peer',None):
			try:
				self.connect(self.peer)
			except:
				self.state=sigIO.ST_ERROR
				raise IOError('error reconnecting to peer')
		elif self.state==sigIO.ST_ERROR:
			raise IOError('dispatch against closed channel -> %s' % repr(self))
		return QueueDispatcher.dispatch(self,evt,cb=cb)
	def main(self):
		while self._active:
			self.response_wait.acquire()
			try:
				self.req_event=self.in_queue.get(timeout=3600)
			except QEmpty:
				continue
			if self.req_event:
				#print self.req_event
				#OK we got a request...
				headers={
						'host':self.req_event['hostname'],
						'User-Agent':'Tao Service Framework',
						'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
						'Keep-Alive':300,
						'Connection':'Keep-Alive'
						}
				if self.auth_cache:
					# we have cached credentials... issue them
					headers['Authorization']=self.auth_cache
				headers.update(self.req_event['req_headers'])
				if self.req_event['req_body']:
					headers['Content-Length']=len(self.req_event['req_body'])
				if self.req_event['req_query']:
					self.req_event['req_path']=self.req_event['req_path']+'?'+urllib.urlencode(self.req_event['req_query'],True)
				self.producer.append('%s %s HTTP/%s\r\n' % (self.req_event['req_type'],httpGetUri(self.req_event),self.req_event['req_version']))
				req=['%s %s HTTP/%s' % (self.req_event['req_type'],httpGetUri(self.req_event),self.req_event['req_version'])]
				if headers.has_key('Proxy-Connection'):
					del(headers['Proxy-Connection'])
				for header in headers.keys():
					req.append('%s: %s' % (header,str(headers[header])))
					self.producer.append('%s: %s\r\n' % (header,str(headers[header])))
				self.producer.append('\r\n')
				if self.req_event['req_body']:
					self.producer.append(self.req_event['req_body'])
				#print '\n'.join(req)
				#print "sending"
				self.doSend()
	def onPacket(self,resp,head={},body=''):
		# we go overboard with the try / except here because...
		# we should handle any and all errors here via dispatch exception handling
		# we have returned True in dispatch so we _have_ to ensure finish gets called _no matter what_
		try:
			#print '[RESPONSE] %s -> %s %s' % (httpGetUri(self.req_event),resp,head)
			if self.req_event['req_headers'].has_key("Authorization"):
				if resp[1]=='200':
					# successful authentication
					self.auth_cache=self.req_event['req_headers']['Authorization']
					self.req_event['validated']=True
				else:
					self.auth_cache=None
			self.req_event['resp_headers']=head
			self.req_event['resp_body']=body
			self.req_event['resp_code']=int(resp[1])
			QueueDispatcher.finish(self,self.req_event)
		except Exception,err:
			self.req_event['resp_body']=traceback.format_exc()
			QueueDispatcher.finish(self,self.req_event,exc=DispatchException(self))
		self.response_wait.release()

class HttpClientProtocol(HttpProtocol):
	__name__="HttpClient"
	# now for the client specific stuff
	def __pinit__(self,**kwargs):
		HttpProtocol.__pinit__(self,**kwargs)
		self.pending=None
	def onPacket(self,resp,head={},body=""):
		""" this will do nicely for testing... but really should be a map of req -> callbacks"""
		#print "onPacket",self.pending
		self.pending(resp[1],resp[2],headers=head,body=self.body_part)
		self.pending=None
	def request(self,method,path,query={},req_headers={},body=None):
		if self.state!=sigIO.ST_CONNECTED:
			raise IOError("Request on unconnected channel")
		headers={
				#'Host':'bits.wikimedia.org',
				#'User-Agent':'Tao Service Framework',
				#'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
				'Keep-Alive':300,
				'Connection':'Keep-Alive'
				}
		headers.update(req_headers)
		if body:
			headers['Content-Length']=len(body)
		if query:
			path=path+'?'+urllib.urlencode(query,True)
		if self.peer[0] in path:
			#print 'REQUEST ->','%s %s HTTP/1.1\r\n' % (method.upper(),path)
			self.producer.append('%s %s HTTP/1.1\r\n' % (method.upper(),path))
		else:
			#print 'REQUEST ->','%s %s HTTP/1.1\r\n' % (method.upper(),path)
			self.producer.append('%s %s HTTP/1.1\r\n' % (method.upper(),path))
		if headers.has_key('Proxy-Connection'):
			del(headers['Proxy-Connection'])
		for header in headers.keys():
			#print "req_header->",'%s: %s' % (header,str(headers[header]))
			self.producer.append('%s: %s\r\n' % (header,str(headers[header])))
		self.producer.append('\r\n')
		if body:
			self.producer.append(body)
		self.doSend()
	def onClose(self,force=False):
		# this catches servers which don't do content-length and instead use connection close to signal end of body
		if self.curr_request:
			self.onPacket(self.curr_request,head=self.curr_headers,body=self.body_part)
			self.curr_request=None
	# couple of quick shortcuts for testing....
	def get(self,path,cb):
		if not self.pending:
			self.pending=cb
			self.request('GET',path)
	def head(self,path,cb):
		if not self.pending:
			self.pending=cb
			self.request('HEAD',path)		
	def post(self,path,data,cb):
		if not self.pending:
			self.pending=cb
			self.request('POST',path,body=data)

class HttpServerProtocol(HttpProtocol):
	__name__="HttpServer"
	def __pinit__(self,**p_args):
		HttpProtocol.__pinit__(self,**p_args)
		self.req_queue=[]
	def onPacket(self,req,head={},body=""):
		#print "putting event"
		#print "HttpServerProtocol.onPacket>>",req,head
		if '?' in req[1]:
			uri,query=req[1].split('?',1)
			query=urlparse.parse_qs(query)
		elif head.has_key('content-type'):
			uri=req[1]
			if head['content-type']=='application/x-www-form-urlencoded':
				query=urlparse.parse_qs(body)
				body=""
			elif head['content-type'].startswith('multipart/form-data'):
				#print 'HttpServerProtocol.onPacket ->',head['content-type'].split(';')
				for params in head['content-type'].split(';')[1:]:
					k,v=params.strip().split('=')
					if k.lower()=='boundary':
						boundary=v
						break
				if boundary:
					query=parseMultipart(body,boundary)
					body=""
			elif head['content-type'].startswith('application/json'):
				if ';' in head['content-type']:
					enc=head['content-type'].split(';')[1].strip()
				else:
					enc='UTF-8'
				query=json.loads(body,enc)
				if type(query)!=dict:
					query={'query':query}
				body=""
			else:
				query=None
		else:
			uri=req[1]
			query={}
		res=urlparse.urlparse(uri)
		#print '>>>>'+body
		if not res.netloc:
			if head.has_key('host'):
				hostname=head['host']
			else:
				try:
					addr,port=self.dev.getsockname()
					if port!=80:
						hostname="%s:%s" % (socket.getfqdn(addr),port)
					else:
						hostname=socket.getfqdn(addr)
				except:
					# final roll of the dice....
					hostname=socket.getfqdn()
			uri='http://%s%s' % (hostname,uri)
		#print "put to.....",req,self._dispatcher
		#print "headers",head
		print "Request from",self.peer
		evt=HttpRequest(req[0],uri,req_version=req[2],headers=head,body=body,req_query=query)
		evt._render=False
		# attempt to set ssl peer data 
		try:
			evt['peer_data']=self.doValidate()
		except:
			# depending on the transport class this may not even exist...
			# swallow all errors here
			pass
		self.req_queue.append(evt)
		try:
			self.put(evt,sync=True)
		except:
			print "exception on put....."
			traceback.print_exc()
	def onCallback(self,event,exc=None):
		"""Render a (possibly handled) event back to the client"""
		#print "Render ->",event['resp_code'],repr(exc)
		if exc:
			print repr(exc)
			if not isinstance(exc,StopIteration):
				event['resp_code']=500
				#event['resp_body']=repr(exc)
				#event['resp_type']='text/plain'
		if not event['resp_code']:
			event['resp_code']=404
		event._render=True
		self.runReqQueue()
	def runReqQueue(self):
		'''this is to cope with pipelining, responses must go out
		in the same order they came in from the client, so run the queue until we come accross
		an unhandled event, then break'''
		while len(self.req_queue):
			if self.req_queue[0]._render:
				event=self.req_queue.pop(0)
			else:
				# event hasn't been dealt with yet
				break
			headers=event['resp_headers']
			headers['Server']='Tao Service Framework v0.1'
			headers['Date']=' %s' % httpDate()
			if event['resp_type']:
				headers['content-type']=event['resp_type']
			elif not headers.has_key('content-type'):
				# TODO: guess the content type.....
				print "TODO: guess content type..."
			if event['req_headers'].has_key('keep-alive'):
				# client will allow keep-alive...
				headers['connection']='keep-alive'
			if event['resp_body']:
				body=event['resp_body']
			elif error_pages.has_key(event['resp_code']):
				body=error_pages[event['resp_code']] % event['req_path']
				headers['content-type']='text/html'
			#elif exc:
			#	event['resp_code']=500
			#	body=error_pages[500]
			#	headers['Content-Type']='text/html'
			else:
				body=''
			# close connection on all errors except 404's
			if event['resp_code']>399 and event['resp_code']!=404:
				headers['connection']='close'
			if event.has_key('resp_msg'):
				#somebody wanted a custom message....
				resp_msg=event['resp_msg']
			else:
				resp_msg=resp_message[event['resp_code']]
			if event['http1']:
				self.producer.append('HTTP/1.1 %s %s\r\n' % (event['resp_code'],resp_msg))
			else:
				self.producer.append('HTTP/0.9 %s %s\r\n' % (event['resp_code'],resp_msg))
			for header in headers.keys():
				if header!='content-length':
					# don't do conent-length here.... it must be the last header..
					self.producer.append('%s:%s\r\n' % (header,headers[header]))
			# this should check if body is a generator....
			# and if so should use TE - Chunked
			if headers.has_key('content-length'):
				#print "set content length -",headers['Content-Length']
				# this is a producer..... 
				self.producer.append('Content-Length:%s\r\n\r\n' % (headers['content-length']))
				self.producer.append(body)
				self.producer.append('\r\n')
			elif type(body)==types.GeneratorType or type(body)==list:
				self.producer.append('Transfer-Encoding:chunked\r\n\r\n')
				self.producer.append(chunkedProducer(body))
				self.producer.append('0\r\n\r\n')
			else:
				self.producer.append('Content-Length: %s\r\n\r\n' % len(body))
				self.producer.append(body+'\r\n')
			self.doSend()

if __name__=="__main__":
	from tao_core import sigIO_net
	import os
	io_main=sigIO.getIO()
	io_main.start()
	if '-server' in sys.argv:
		# real quick and dirty dispatcher....
		def httpDispatch(event,cb=None):
			print "Http Request ->",event['req_path'],cb
			print "Request Headers ->"
			for key in event['req_headers'].keys():
				print "\t%s -> %s" % (key,event['req_headers'][key])
			event['resp_code']=200
			#event['resp_message']='OK'
			event['resp_body']="<html><body>test page - %s</body></html>" % event['req_path']
		chan=io_main.newChannel(HttpServerProtocol,sigIO_net.TCPInetTransport,dispatcher=httpDispatch)
		chan.listen((os.getenv('IP','0.0.0.0'),int(os.getenv('PORT',8080))))
		while 1:
			try:
				time.sleep(20)
			except KeyboardInterrupt:
				break
		chan.close()
	else:
		def resp_cb(evt,exc=None):
			if exc:
				print exc
			print "Response -> %s" % (evt['resp_code'])
			print evt['resp_headers']
			print len(evt['resp_body'])
		c_chan=io_main.newChannel(HttpClientDispatcher,sigIO_net.TCPInetTransport)
		c_chan.connect(('www.google.co.uk',80))
		last=0
		while 1:
			if time.time()-last>20:
				print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>> calling GET"
				#c_chan.get('/en.wikipedia.org/load.php',cb=resp_cb)
				c_chan.dispatch(HttpRequest(req_type='GET',req_uri='http://www.google.co.uk/'),cb=resp_cb)
				last=time.time()
			try:
				time.sleep(20-(time.time()-last))
			except KeyboardInterrupt:
				break
		c_chan.close()
	io_main.stop()


