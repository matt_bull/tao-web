from tao_core.tdf_core import MappingDispatcher,TrackDispatcher,dispatchWithCallback,DispatchException
from tao_core import sigIO
from tao_web.http import HttpClientDispatcher,HttpRequest,httpGetUri,parseCookie
from tao_core.sigIO_net import TCPInetTransport, TLSInetTransport
import urlparse

io_main=None

def getHttpClient(host,port=80,ssl=False):
	global io_main
	if not io_main:
		io_main=sigIO.getIO()
		io_main.start()
	if ssl:
		ch=io_main.newChannel(HttpClientDispatcher,TLSInetTransport)
	else:
		ch=io_main.newChannel(HttpClientDispatcher,TCPInetTransport)
	ch.connect((host,port))
	return ch

class ClientDispatcher(MappingDispatcher,TrackDispatcher):
	def __init__(self):
		MappingDispatcher.__init__(self)
		TrackDispatcher.__init__(self)
		self.cookie_cache={}
	def resolveCookies(self,host,path):
		r=[]
		for k_host,k_path in self.cookie_cache.keys():
			if host.endswith(k_host) and path.startswith(k_path):
				r.append(self.cookie_cache[(k_host,k_path)])
		return r
	def dispatch(self,event,cb=None):
		#TrackDispatcher.dispatch(self,event,cb=cb)
		self.cb_map[event]=cb
		if self.dispatch_map.has_key((event['hostname'],event['port'])):
			try:
				dis_res=self.dispatch_map[(event['hostname'],event['port'])](event,cb=self.finish)
				#print ">>dispatched>>",event,dispatcher,dis_res
			except StopIteration:
				self.finish(event)
				#print ">>dispatched>>",event,dispatcher,'StopIteration'
			except IOError:
				del(self.dispatch_map[(event['hostname'],event['port'])])
			except Exception,exc:
				self.finish(event,exc=DispatchException(dispatcher))
				#print ">>dispatched>>",event,dispatcher,exc
			else:
				if not dis_res:
					cb(event)
				return dis_res
		try:
			if event['scheme']=='https':
				self.registerDispatcher((event['hostname'],event['port']),getHttpClient(event['hostname'],port=event['port'],ssl=True))
			else:
				self.registerDispatcher((event['hostname'],event['port']),getHttpClient(event['hostname'],port=event['port']))
			cookies=self.resolveCookies(event['hostname'],event['req_path'])
			if cookies:
				event['req_headers']['Cookie']=';'.join(cookies)
		except Exception,exc:
			self.finish(event,exc=DispatchException(self))
			return
		#print "dispatch to %s -> %s-%s" % (self.dispatch_map[(event['hostname'],event['port'])],event['req_type'],httpGetUri(event))
		try:
			return dispatchWithCallback(self.dispatch_map[(event['hostname'],event['port'])],event,self.finish)
		except ReferenceError:
			del(self.dispatch_map[(event['hostname'],event['port'])])
	def registerDispatcher(self,key,dispatcher):
		if isinstance(dispatcher,HttpClientDispatcher):
			MappingDispatcher.registerDispatcher(self,key,dispatcher)
		else:
			raise TypeError('ClientDispatcher can only map to HttpClientDispatcher instances')
	def finish(self,event,exc=None):
		if exc:
			TrackDispatcher.finish(self,event,exc=exc)
		else:
			if event['resp_headers'].has_key('Set-Cookie'):
				for cookie in event['resp_headers']['Set-Cookie']:
					cookie,c_attr=parseCookie(cookie)
					if not c_attr.has_key('path'):
						c_attr['path']=event['req_path']
					if not c_attr.has_key('domain'):
						c_attr['domain']=event['hostname']
					self.cookie_cache[(c_attr['domain'],c_attr['path'])]=cookie
			if event['resp_code'] in (301,302,307):
				# redirect.... check we are not redirecting back into the same client dispatcher.
				sch,host,rpath,para,query,frag=urlparse.urlparse(event['resp_headers']['location'],scheme='http')
				print "Redirect to %s://%s/%s" % (sch,host,rpath)
				if ':' in host:
					host,port=host.split(':',1)
					port=int(port)
				elif sch=='https':
					port=443
				else:
					port=80
				print "Connect on redirect %s:%s" % (host,port)
				if event['hostname']==host and event['port']==port:
					# ugh redirect to same hostname and port.... avoid the dispatch from callback trap here...
					TrackDispatcher.finish(self,event,exc=Exception('unable to follow redirect to same server'))
				else:
					cb=self.cb_map.pop(event)
					event['scheme']=sch
					event['hostname']=host
					event['port']=port
					event['req_path']=rpath
					self.dispatch(event,cb=cb)
			else:
				TrackDispatcher.finish(self,event)

if __name__=='__main__':
	import time
	def resp_cb(evt,exc=None):
		if exc:
			print '%s\n\t%s\n\tResponse -> %s\n%s' % (httpGetUri(evt),repr(exc),(evt['resp_code']),evt['resp_headers'])
		else:
			#print evt['resp_body']
			if not evt['resp_body']:
				evt['resp_body']=''
			print httpGetUri(evt),"\n\tResponse -> %s" % (evt['resp_code']),'\n\t',evt['resp_headers'],'\n\t',len(evt['resp_body'])
	cd=ClientDispatcher()
	for uri in ['https://www.wikipedia.org/','https://www.python.org/','https://www.google.co.uk/','http://www.amazon.co.uk/']:
		print "[STARTING] -> %s" % uri
		dispatchWithCallback(cd,HttpRequest(req_type='GET',req_uri=uri,req_version='1.1'),cb=resp_cb)
		time.sleep(1000)
	#cd.dispatch(HttpRequest(req_type='GET',req_uri='https://www.python.org/'),cb=resp_cb)
	while 1:
		try:
			time.sleep(1)
		except KeyboardInterrupt:
			break
