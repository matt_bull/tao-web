from http import *
from tao_core.tdf_core import Dispatcher
from hashlib import md5
import sys,base64,os.path,traceback

#this is just for example
sys.path.append('../')

from tao_core.tdf_core import *
from tao_core.sigIO import FileProducer

extension_map={
		('htm','html'):'text/html',
		('txt'):'text/utf-8'
	}

# add an error page for 401....
error_pages[401]='<html><head><title>401 - Authorization Required</title></head><body><font family=arial,helvetica><h2>401 - Authorization Required</h2>Authentication required to access the requested document %s</font></body></html>'

class HttpLogDispatcher(TrackDispatcher):
	""" obviously this could / should be a lot more complex, do log rotation etc"""
	def __init__(self,dispatcher,log_path):
		TrackDispatcher.__init__(self)
		self._dispatcher=dispatcher
		self.log_file=open(log_path,'a')
	def dispatch(self,event,cb=None):
		print "LOG::",httpDate(),"Request",event['req_type'],event['req_path']
		self.log_file.write("%s::%s -> %s" % (httpDate(),event['req_type'],event['req_path']))
		return TrackDispatcher.dispatch(self,event,cb=cb)
	def finish(self,event,exc=None):
		# TODO: log to self.log_file in standard (ie apache log format....)
		print "LOG::",httpDate(),"Response",event['req_path'],event['resp_code'],event['resp_type'],repr(exc)
		#print event['resp_headers']
		self.log_file.write("%s::%s <- %s" % (httpDate(),event['req_path'],event['resp_code']))
		TrackDispatcher.finish(self,event,exc=exc)
	def __del__(self):
		# clean up the log file....
		self.log_file.close()

class HttpVHostDispatcher(Dispatcher):
	def __init__(self):
		Dispatcher.__init__(self)
		self.vhost_map={}
	def registerDispatcher(self,vhost,disp):
		# check a dispatcher doesn't exist...
		if self.vhost_map.has_key(vhost):
			raise KeyError("dispatcher already registered for virtual host %s" % vhost)
		self.vhost_map[vhost]=disp
	def removeDispatcher(self,vhost,disp):
		del(self.vhost_map[vhost])
	def dispatch(self,event,cb=None):
		if self.vhost_map.has_key(event['req_headers']['host'].strip()):
			return self.vhost_map[event['req_headers']['host'].strip()](event,cb=cb)
		else:
			print "[ERR] unknown vhost",event['req_headers']['host'],self.vhost_map

class HttpAuthDispatcher(TrackDispatcher):
	"""dispatcher which wraps an authenticator (see auth_core), 
	implementing all the http specific parts (401 response etc)"""
	def __init__(self,dispatcher,auth=None,realm=None):
		TrackDispatcher.__init__(self)
		if auth:
			# authenticator is an instance of BaseAuthenticator or a subclass...
			self._dispatcher=auth
			self._dispatcher.setDispatcher(dispatcher)
		else:
			# no authenticator passed in assume there is an authenticator further on in the dispatch graph...
			self._dispatcher=dispatcher
		self.realm=realm
	def dispatch(self,evt,cb=None):
		# put in the keys auth dispatchers expect....
		if self.realm:
			evt['realm']=self.realm
		else:
			evt['realm']=evt['req_path']
		evt['identity']=''
		evt['credentials']=None
		evt['validated']=False
		if evt['req_headers'].has_key('authorization'):
			if evt['req_headers']['authorization'].strip().startswith('basic'):
				# do basic auth....
				cred_str=evt['req_headers']['authorization'].strip().split(' ',1)[1]
				uname,cred=base64.b64decode(cred_str).split(':',1)
				evt['identity']=uname
				evt['credentials']=cred
			# TODO: implement http digest auth
		return TrackDispatcher.dispatch(self,evt,cb=cb)
		#return dispatchWithCallback(self._dispatcher,evt,self.finish)
	def finish(self,evt,exc=None):
		##print "!!! in HttpAuth",exc
		if not evt['validated']:
			evt['resp_code']=401
			# this should be changed depending on evt['auth_method']
			evt['resp_headers']={'WWW-Authenticate':'Basic realm="%s"' % evt['realm']}
			evt['resp_body']=error_pages[401] % evt['req_path']
		TrackDispatcher.finish(self,evt,exc=exc)

class UserProfileAuth(TrackDispatcher):
	# http dispatcher to do html authentication etc
	# should be introduced _before_ the actual auth dispatcher
	def __init__(self,dispatcher,cache={},resources_path=None):
		TrackDispatcher.__init__(self)
		self.cookie_cache=cache
		self._dispatcher=dispatcher
		self.timeout=24*60*60
		if resources_path:
			self.respath=resources_path
		else:
			self.respath=os.path.split(os.path.abspath(sys.modules[__name__].__file__))[0]
	def dispatch(self,evt,cb=None):
		if '_login_res_' in evt['req_path']:
			path=evt['req_path'].split('_login_res_')[-1].strip('/')
			try:
				evt['resp_type'],evt['resp_body']=self.getLoginResource(path.split('/'))
				evt['resp_code']=200
			except:
				evt['resp_code']=404
				traceback.print_exc()
			return
		elif evt['req_path'].strip('/')=='_exit_' and evt['req_headers'].has_key('cookie'):
			# logout request...
			for cstr in evt['req_headers']['cookie'].split(';'):
				try:
					k,v=cstr.split('=')
					if k.strip()=='SID':
						del(self.cookie_cache[v])
				except:
					pass
			#evt['resp_code']=200
			#evt['resp_body']='<html><head><title>Logged Out</title></head><body><font family=arial,helvetica><h2>Logout</h2>you are now logged out</font></body></html>'
			evt['resp_type'],evt['resp_body']=self.getLoginPage(evt['hostname'],req_uri='/',message='Logged Out')
			evt['resp_headers']['Content-Location']=httpGetUri(evt,req_path='/login')
			evt['resp_code']=200
			return
		else:
			evt['validated']=False
			last_ts=0
			if evt['req_headers'].has_key('cookie'):
				for cstr in evt['req_headers']['cookie'].split(';'):
					try:
						k,v=cstr.split('=')
						if k.strip()=='SID':
							uid,cred,last_ts=self.cookie_cache[v]
							if time.time()>last_ts+self.timeout:
								del(self.cookie_cache[v])
							else:
								evt['identity']=uid
								evt['credentials']=cred
								evt['cookie']=v
					except:
						pass
			if evt['req_query'].has_key('username'):
				evt['identity'],evt['credentials']=self.parseQuery(evt['req_query'])
		if not evt.has_key('identity'):
			#initial load return login page
			evt['resp_type'],evt['resp_body']=self.getLoginPage(evt['hostname'],req_uri=evt.getUri())
			evt['resp_code']=200
		return TrackDispatcher.dispatch(self,evt,cb=cb)
	def getLoginResource(self,path):
		# this is something embedded in the page....
		f=open(os.path.join(self.respath,req_path[1:]),'r')
		return (None,f.read())
	def finish(self,evt,exc=None):
		#print '>>>>',evt['validated'],evt['resp_code']
		#try:
		if evt.has_key('validated') and evt['validated']:
			if evt.has_key('cookie'):
				cookie=self.updateCookie(evt['identity'],evt['credentials'],cookie=evt['cookie'])
			else:
				cookie=self.updateCookie(evt['identity'],evt['credentials'])
			evt['resp_headers']['Set-Cookie']='SID=%s; Domain=%s; Expires=%s; HttpOnly;' % (cookie,evt['hostname'],httpDate(offset=self.timeout))
		elif evt.has_key('auth_method') and evt['auth_method']=='plain':
			# return login page.... clear cache of any cookie for this identity....
			if evt['identity']:
				# failed auth attempt
				for key,val in self.cookie_cache.items()[:]:
					if val[0]==evt['identity']:
						del(self.cookie_cache[key])
			#	evt['resp_type'],evt['resp_body']=self.getLoginPage(evt['hostname'],req_path=evt['req_path'])
			#else:
			evt['resp_type'],evt['resp_body']=self.getLoginPage(evt['hostname'],req_uri=evt.getUri(),message='Incorrect Username or Password, please check and try again')
			evt['resp_code']=200
		else:
			#??? maybe the upstream dispatcher doesn't understand??
			if evt['resp_code']==404 and evt.has_key('cookie'):
				# we didn't hit a dispatcher and we have been previously authed... do 404
				pass
			else:
				# back to the login page with you, prevents probing for site structure...
				evt['resp_type'],evt['resp_body']=self.getLoginPage(evt['hostname'],req_uri=evt.getUri(),message='Incorrect Username or Password, please check and try again')
				evt['resp_code']=200
		#except Exception,err:
			# whatever happens do not allow an exception to bypass security....
		#		evt['resp_code']=500
		#		evt['resp_body']=repr(err)
		#		raise err
		#print "calling TrackDispatcher.finish"
		TrackDispatcher.finish(self,evt,exc=exc)
	def parseQuery(self,query):
		# return (identity,credentials) tuple parsed from query (generated by login page)
		return (query['username'][0],query['password'][0])
	def updateCookie(self,uid,cred,cookie=None):
		# override to update (or create) cookie for uid and cred
		if not cookie:
			cookie=md5(str(uid)+str(time.time())).hexdigest()
		self.cookie_cache[cookie]=(uid,cred,time.time())
		return cookie
	def getLoginPage(self,hostname,req_uri=None,message=''):
		# override to return login page.... (or resource...)
		# return (<mime type>,<body>) tuple
		pass

#class HttpOpenIDAuthDispatcher(Dispatcher):
#	def __init__(self,user_db,):

# there is a problem with path mapping the dispatchers (or more precisely nesting the path maps)....
# this nests neatly by looking at the paths during registerDispatcher which will raise KeyError if...
# a dispatcher exists for a more specific path ie. when registering a dispatcher for '/foo' when a dispatcher for '/foo/bar' already exists
# a dispatcher exists for a less specific path which is not also an instance of HttpPathDispatcher ie. registering a dispatcher for /foo/bar when one exists for /foo
# if the dispatcher _is_ an path dispatcher it will attempt to register your dispatcher with that...

class HttpPathDispatcher(MappingDispatcher):
	accept_events=['org.digitao.tao_web.HttpRequest']
	def __init__(self,default_dispatcher=None):
		super(HttpPathDispatcher,self).__init__()
		self.default=default_dispatcher
	def registerDispatcher(self,path,disp):
		# only allow one dispatcher per path and catch subpaths
		for epath in self.dispatch_map.keys():
			if epath.startswith(path):
				raise KeyError('duplicate dispatcher exists for path %s with path %s' % (path,epath))
			if path.startswith(epath):
				# this may be another path dispatcher if so register it with that....
				if isinstance(self.dispatch_map[epath],HttpPathDispatcher):
					path=path[len(epath):]
					return self.dispatch_map[epath].registerDispatcher(path,disp)
				else:
					raise KeyError('duplicate dispatcher exists for subpath %s of path %s' % (path,epath))
		# no dups register dispatcher
		self.dispatch_map[path]=disp
	def removeDispatcher(self,path,disp):
		if self.dispatch_map.has_key(path):
			del(self.dispatch_map[path])
		else:
			for epath in self.dispatch_map.keys():
				if path.startswith(epath) and isinstance(self.dispatch_map[epath],HttpPathDispatcher):
					# aha this may have been passed on....
					self.dispatch_map[epath].removeDispatcher(path,disp)
	def dispatch(self,event,cb=None):
		for key in self.dispatch_map.keys()[:]:
			#print "HttpPathDispatcher.dispatch",event['disp_path'],key
			if httpMatchPath(event,key):
				#print "Path Matched!!!!"
				try:
					return self.dispatch_map[key](event,cb=cb)
				except ReferenceError:
					#print 'ref error in path map'
					del(self.dispatch_map[key])
		if self.default:
			return self.default(event,cb=cb)

class QueryPathDispatcher(Dispatcher):
	accept_events=['org.digitao.tao_web.HttpRequest']
	def __init__(self,dispatcher,key='path'):
		Dispatcher.__init__(self,dispatcher=dispatcher)
		self.key=key
	def dispatch(self,evt,cb=None):
		elem=evt['disp_path'].split('/',1)
		if len(elem)>1:
			evt['disp_path']=elem[1]
		else:
			evt['disp_path']=''
		evt['req_query'][self.key]=elem[0]

class HttpUserPathDispatcher(HttpPathDispatcher,TrackDispatcher):
	def __init__(self,default_dispatcher=None,home_base='/home',export_folder='Public'):
		HttpPathDispatcher.__init__(self,default_dispatcher=default_dispatcher)
		TrackDispatcher.__init__(self)
		self.home_base=home_base
		self.folder=export_folder
	def dispatch(self,event,cb=None):
		if event['req_path'].startswith('~'):
			# manipulate path create fs dispatcher on demand if needed and pass in our callback
			# so we can 'repair' the path on the way back
			tl,target_user,path=event['req_path'].split('/',2)
			if not self.dispatch_map.has_key(target_user):
				if os.access(os.path.join((self.home_base,target_user,self.folder)),os.R_OK):
					fs_dispatcher=HttpFsDispatcher(os.path.join((self.home_base,target_user,self.folder)))
					self.registerDispatcher(target_user,fs_dispatcher)
				else:
					event['resp_code']=404
					raise StopIteration()
			event['req_path']=path
			# cache target user in event so we can reconstruct path later
			event['target_user']=target_user
			return TrackDispatcher.dispatch(self,event,cb=cb)
			#return dispatchWithCallback(self.dispatch_map[target_user],event,self.finish)
		else:
			return self.default(event,cb=cb)
	def finish(self,event,exc=None):
		# reconstitute the req path before doing callback
		event['req_path']='~/'+event['target_user']+'/'+event['req_path']
		TrackDispatcher.finish(self,event,exc=exc)

class HttpFsDispatcher(Dispatcher):
	def __init__(self,base_path):
		super(HttpFsDispatcher,self).__init__()
		self.base_path=base_path
	def dispatch(self,event,cb=None):
		# we never return True so we can ignore cb
		# check request type... we only handle GET
		#print event['req_type'],os.path.abspath(self.base_path)
		#if event['req_type']!="GET":
		#	print "ignoring request type %s" % event['req_type']
		#	return False
		# get the relative path from disp_path
		path=os.path.join(*event['disp_path'])
		# then check the file exists
		#print "CHECKING :",os.path.join(self.base_path,path)
		#print "HEADERS  :",event['req_headers']
		if path and (not os.access(os.path.join(self.base_path,path),os.R_OK)):
			# we have no access to the file abort and let other handlers have a crack...
			#print "no file!!!"
			#print event['resp_headers']
			return			
		elif not path:
			if os.access(os.path.join(self.base_path,'index.html'),os.R_OK):
				path='index.html'
			elif os.access(os.path.join(self.base_path,'index.htm'),os.R_OK):
				path='index.htm'
			elif os.access(os.path.join(self.base_path,'default.htm'),os.R_OK):
				path='default.htm'
			else:
				# no access to default filename either... bail
				return
		p_f=open(os.path.join(self.base_path,path),'r')
		# there must be a better way of doing this???
		p_f.seek(0,os.SEEK_END)
		# but we must set file length.....
		event['resp_headers']['Content-Length']=p_f.tell()
		#print "file length ->",p_f.tell()
		p_f.seek(0)
		f_typ=self.guessType(p_f,path)
		if f_typ:
			event['resp_type']=f_typ
		#print event['resp_headers']
		event['resp_body']=FileProducer(p_f)
		#print repr(event['resp_body'])
		event['resp_code']=200
		#event['resp_message']="OK"
		#print "raising stopIteration"
		raise StopIteration()
	def guessType(self,f_obj,path):
		ext=path.rsplit('.',1)[1]
		for exten_grp in extension_map:
			if ext in exten_grp:
				return extension_map[exten_grp]

class ArgPathDispatcher(Dispatcher):
	accept_events=["org.digitao.tao_web.HttpRequest"]
	# a dispatcher which extracts an argument out of the next path element
	# eg you wanted a path layout like.... 
	# /users/<user_id>/
	#					summary/
	#					edit/
	# use ArgPathDispatcher then add a HttpPathDispatcher and in your views use the 'user_id' key in req_query
	def __init__(self,dispatcher=None,req_key='path_elem'):
		Dispatcher.__init__(self,dispatcher=dispatcher)
		self.req_key=req_key
	def dispatch(self,evt,cb=None):
		if not evt['req_query'].has_key(self.req_key):
			evt['req_query'][self.req_key]=evt['disp_path'].pop(0)
		#print evt['req_query']
		return Dispatcher.dispatch(self,evt,cb=cb)

class Endpoint(EventAdapter):
	def getEvent(self,http_evt):
		# override to get adapted event for http_evt
		raise NotImplementedError("no getEvent method in adapter")
	def adaptResponse(self,n_evt,http_evt,exc):
		#print 'adapt<<<<',n_evt,exc
		try:
			http_evt['validated']=n_evt['validated']
			if n_evt['validated']:
				http_evt['resp_code']=200
				# TODO: refactor out the uri handling into an http event method??
				http_evt['resp_body']="<html><body>%s</body></html>" % repr(n_evt.getDict())
				#http_evt['resp_body']=self.render("http://%s%s" % (http_evt['hostname'],http_evt['req_path']),n_evt.getDict(),exc=exc)
		except:
			# set response code to 500
			http_evt['resp_code']=500
	def render(self,doc_uri,data,exc=None):
		# override to return rendered template for event
		raise NotImplementedError()

if __name__=="__main__":
	from tao_core import sigIO_net
	io_main=sigIO.getIO()
	io_main.start()
	# assemble our dispatch graph.....
	# create a vhost dispatcher...
	http_vh=HttpVHostDispatcher()
	# create an fs dispatcher for pwd
	http_fs=HttpFsDispatcher('./')
	# add that as localhost....
	http_vh.registerDispatcher('localhost',http_fs)
	# create a dispatcher for our home directory...
	http_home=HttpFsDispatcher('/home/mbull/Public/')
	# 
	
	# register as virtual host http_test.local
	http_vh.registerDispatcher('http_test.local',http_home)
	# put a log dispatcher on the front end....
	http_log=HttpLogDispatcher(http_vh,os.path.expanduser('~/http_log'))
	# feed that into our channel
	chan=io_main.newChannel(HttpServerProtocol,sigIO_net.TCPInetTransport,dispatcher=http_log)
	chan.listen(('localhost',80))
	while 1:
		try:
			time.sleep(20)
		except KeyboardInterrupt:
			break
	chan.close()
	io_main.stop()

