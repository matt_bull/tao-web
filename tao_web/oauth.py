from tao_core.tdf_core import TrackDispatcher,Dispatcher,QueueDispatcher,DispatchException,dispatchWithCallback
from tao_web.http import httpDate,httpMatchPath,httpGetUri,HttpRequest
from tao_web.http_dispatch import UserProfileAuth
from urlparse import urlparse
from tao_web.http_client import ClientDispatcher,getHttpClient
from Queue import Empty
import hashlib,time,urllib,json,thread

TOK_AUTH=0
TOK_ACC=1
TOK_REFR=2

AS_NOA=0 	# not authenticated
AS_INP=1	# auth in progress
AS_AUTH=2	# got auth token
AS_ACC=3	# got access token

class OAuthError(DispatchException):
	def __init__(self,dispatcher,oath_error={}):
		DispatchException.__init__(self,dispatcher)
		self.oauth_error=oath_error
		

class OAuthClientDetail(dict):
	def __init__(self,client_id,**kwargs):
		dargs={
			'grant_type':'authorisation_code',
			'scopes':[],
			'redirect_uris':[]
			}
		dargs.update(kwargs)
		dict.__init__(self,dargs)
		self.client_id=client_id

class OAuthServer(UserProfileAuth):
	"""
	Fullfils the roles of both Resource server and Authorization server
	as defined in RFC 6749
	"""
	def __init__(self,dispatcher=None,client_map={}):
		UserProfileAuth.__init__(self,dispatcher=dispatcher)
		# client_map should be mapping of client ID's to OAuthClientDetail instances
		self.client_map={}
		for id,detail in client_map.items():
			if not isinstance(detail,OAuthClientDetail):
				raise TypeError("values of client_map must be OAuthClientDetail instances")
			self.client_map[id]=detail
		self.state_map={}
		self.token_cache={}
	def dispatch(self,event,cb=None):
		if httpMatchPath(event,'token'):
			# token endpoint....
			# default to error code....
			event['resp_code']=400
			event['resp_type']='application/json;charset=UTF-8'
			#turn off caching
			event['resp_headers'].update({'Cache-Control':'no-store','Pragma':'no-cache'})
			if event['req_query']['grant_type']=='authorization_code':
				# auth / access token exchange...
				query=event['req_query']
				if self.token_cache.has_key(query['code']):
					token=self.token_cache[query['code']]
					if token[2]==query['client_id'] and token[3]==query['redirect_uri']:
						if time.time()<token[4]:
							# cool we are within time...
							event['resp_body']=json.dumps({
									"access_token":self.generateToken(TOK_ACC,token[1],token[2]),
									"token_type":"bearer",
									"expires_in":3600,
									"refresh_token":self.generateToken(TOK_REFR,token[1],token[2])
									})
							event['resp_code']=200
						else:
							event['resp_body']=json.dumps({'error':'invalid_grant'})
						# either way we can get rid of auth token now...
						del(self.token_cache[query['code']])
					event['resp_body']=json.dumps({'error':'invalid_client'})
				event['resp_body']=json.dumps({'error':'invalid_grant'})
			elif event['req_query']['grant_type']=='refresh_token':
				if self.token_cache.has_key(event['req_query']['refresh_token']):
					token=self.token_cache.pop(event['req_query']['refresh_token'])
					#	if time.time()<token[4]: can refresh tokens timeout??
							# cool we are within time...
					event['resp_body']=json.dumps({
							"access_token":self.generateToken(TOK_ACC,token[1],token[2]),
							"token_type":"bearer",
							"expires_in":3600,
							"refresh_token":self.generateToken(TOK_REFR,token[1],token[2])
							})
			raise StopIteration()
		elif event['req_query'].has_key('client_id') and self.client_map.has_key(event['req_query']['client_id']):
			# client redirected a user agent here.... check client id and redirect uri
			if event['req_query']['redirect_uri'] in self.client_map[event['req_query']['client_id']]['redirect_uris']:
				if self.state_map.has_key(event['req_query']['state']):
					#ouch bail.... should really deal with this gracefully...
					raise KeyError("duplicate state key")
				self.state_map[event['req_query']['state']]=(event['req_query']['client_id'],event['req_query']['redirect_uri'])
				# this is an authentication request.... send out the login page
				event['resp_type'],event['resp_body']=self.getLoginPage(event['req_query']['state'])
				event['resp_headers']['Set-Cookie']='OauthState=%s; Domain=%s; Expires=%s; HttpOnly;' % (event['req_query']['state'],event['hostname'],httpDate(offset=240))
				event['resp_code']=200
			else:
				event['resp_code']=500
				event['resp_body']='incorrect redirect_uri for client'
			raise StopIteration()
		elif  event['req_headers'].has_key('authorization') and event['req_headers']['authorization'].strip().startswith('bearer'):
			# check access token...
			tok_str=event['req_headers']['authorization'].strip().split(' ',1)[1]
			if self.token_cache.has_key(tok_str) and self.token_cache[tok_str][0]==TOK_ACC:
				# should really get scopes for client_id here and add them to event...
				event['identity']=self.token_cache[tok_str][1]
				# this isn't real credentials its the client ID....
				event['credentials']=self.token_cache[tok_str][2]
				event['validated']=True
				# go around UserProfile auth and dispatch directly
				return self._dispatcher.dispatch(event,cb=cb)
		# so none of that.... do UserProfileAuth...
		# after we pick up OAuth cookie values of course...
		for cstr in event['req_headers']['cookie'].split(';'):
			try:
				k,v=cstr.split('=')
				if k.strip()=='OauthState':
					event['state']=v
			except:
				pass
		UserProfileAuth.dispatch(self,event,cb=cb)
	def getLoginPage(self,state):
		#return (resp_type,resp_body) tuple...
		#state _must_ be included in form in hidden form element 
		raise NotImplemented('getLoginPage not implemented in OAuthServer')
	def getScopePage(self,client_detail):
		# override to return (resp_type,resp_body) tuple which will confirm scopes for client if required
		raise NotImplemented('getScopePage not implemented in OAuthServer')
	def generateToken(self,token_type,identity,client_id,redirect_uri=None):
		# return cryptographic key to return as authorization token to client
		hs=hashlib.new('sha1',identity)
		hs.update(client_id)
		if redirect_uri:
			hs.update(redirect_uri)
		hs.update(str(time.time()))
		if token_type==TOK_AUTH:
			self.token_cache[hs.hexdigest()]=(token_type,identity,client_id,redirect_uri,int(time.time()+600))
		else:
			self.token_cache[hs.hexdigest()]=(token_type,identity,client_id,int(time.time()+3600))
		return hs.hexdigest()
	def finish(self,event,exc=None):
		try:
			if event.has_key('state') and event.has_key('validated') and event['validated']:
				client_detail=self.client_map[self.state_map[event['state']][0]]
				# validated response....  redirect to client or scope page
				if client_detail.scopes:
					(event['resp_type'],event['resp_body'])=self.getScopePage(client_detail)
				else:
					event['resp_code']=302
					client_id,redirect_uri=self.state_map.pop(event['state'])
					code=self.generateToken(TOK_AUTH,event['identity'],client_id,redirect_uri)
					event['resp_headers']['Location']='%s?code=%s&state=%s' % (redirect_uri,code,event['state'])
				TrackDispatcher.finish(self,event,exc=exc)
		except Exception,exc:
			# whatever happens do not allow an exception to bypass security....
			if event['state']:
				event['resp_code']=302
				event['resp_headers']['Location']='%s?error=server_error&state=%s' % (self.state_map[event['state']][1],event['state'])
				del(self.state_map[event['state']])
			else:
				event['resp_code']=500
			TrackDispatcher.finish(self,event,exc=exc)
		# so not in an OAuth workflow then.... do UserProfile as usual
		UserProfileAuth.finish(self,event,exc=exc)

# I have a feeling this is all kinds of wrong no hndling of multiple users for example....
class OAuthClient(QueueDispatcher):
	"""Stand alone OAuth2.0 client add in for http event dispatch graphs.
	
	"""
	def __init__(self,dispatcher,auth_uri,client_id,client_auth=None,token_ep_uri=None):
		self.token_cache={'auth_token':None,'access_token':None,'timeout':0,'refresh_token':None}
		if token_ep_uri:
			self.token_ep=token_ep_uri
			tok_uri=urlparse(token_ep_uri)
			if tok_uri.port:
				self.tok_client=getHttpClient(tok_uri.hostname,tok_uri.port,ssl=tok_uri.scheme=='https')
			elif tok_uri.scheme=='https':
				self.tok_client=getHttpClient(tok_uri.hostname,443,ssl=True)
		self.auth_uri=auth_uri
		self.client_id=client_id
		self.client_auth=client_auth
		self._auth_state=AS_NOA
		self._dispatcher=dispatcher
		QueueDispatcher.__init__(self)
		self._hash=hashlib.new('sha1',str(id(self)))
		self._hash.update(self.client_id)
		self._hash.update(str(time.time()))
	def dispatch(self,event,cb=None):
		if self._auth_state==AS_ACC:
			QueueDispatcher.dispatch(self,event,cb=cb)
		if self._auth_state==AS_NOA:
			# not authenticated... start the auth process
			self._auth_state=AS_INP
			event['resp_code']=302
			# TODO: sanely handle scopes
			event['resp_headers']['Location']='%s?response_type=code&%s' % (self.auth_uri,urllib.urlencode({'client_id':self.client_id,'state':self._hash.hexdigest(),'redirect_uri':httpGetUri(event)}))
			raise StopIteration
		elif self._auth_state==AS_INP:
			# looking for the auth token...
			if event['req_query'].has_key('code'):
				# yay we have an auth token...
				# get an access token
				self._auth_state=AS_AUTH
				self.token_cache['auth_token']=event['req_query']['code'][0]
				self.token_cache['redirect_uri']
				tok_req={"grant_type":"authorization_code","client_id":self.client_id,"redirect_uri":httpGetUri(event)}
				tok_request=HttpRequest('POST',self.token_ep)
				tok_request['req_headers']['content-type']="application/x-www-form-urlencoded"
				tok_request['request_body']=urllib.urlencode(tok_req)
				dispatchWithCallback(self.tok_client,HttpRequest('POST',self.token_ep),self.authCb)
			elif event['req_query'].has_key('error'):
				# booo error response from auth server...
				# take us back to non-auth and deliver some kind of explaination page...
				self._auth_state=AS_NOA
	def main(self):
		self.resp_wait=thread.allocate_lock()
		while self._active:
			self.resp_wait.acquire()
			if self.token_cache['access_token'] and time.time()<self.token_cache['timeout']:
				self.curr_event=self.in_queue.get(timeout=self.token_cache['timeout']-time.time())
				if self.curr_event:
					dispatchWithCallback(self._dispatcher,self.curr_event,self.dataCb)
					continue
			# timed out...
			if self.token_cache['refresh_token']:
				self._auth_state=AS_AUTH
				tok_req={"grant_type":"refresh_token","refresh_token":self.token_cache['refresh_token']}
				tok_request=HttpRequest('POST',self.token_ep)
				tok_request['req_headers']['content-type']="application/x-www-form-urlencoded"
				tok_request['request_body']=urllib.urlencode(tok_req)
				dispatchWithCallback(self.tok_client,HttpRequest('POST',self.token_ep),self.authCb)
				#query={'grant_type':["refresh_token"],'refresh_token':[self.token_cache['refresh_token']]}
				#dispatchWithCallback(self.tok_client,HttpRequest('POST',self.auth_uri,req_query=query),self.authCb)
			else:
				self.token_cache['auth_token']=None
				self._auth_state=AS_INP
				try:
					event=self.in_queue.get_nowait()
				except Empty:
					continue
				event['resp_code']=302
				# TODO: sanely handle scopes
				event['resp_headers']['Location']='%s?response_type=code&%s' % (self.auth_uri,urllib.urlencode({'client_id':self.client_id,'state':self._hash.hexdigest(),'redirect_uri':httpGetUri(event)}))
	def dataCb(self,event,exc=None):
		TrackDispatcher.finish(self,event,exc=exc)
		self.resp_wait.release()
	def authCb(self,event,exc=None):
		if exc:
			# TODO: cleanly deal with response here
			pass
		try:
			if self._auth_state==AS_AUTH and event['resp_code']==200:
				# pull everything out of the token...
				tok=json.loads(event['resp_body'])
				if tok['token_type']!='bearer':
					# ouch... we don't implement anything else.. bailout and hope...
					self._auth_state=AS_NOA
					raise TypeError("wrong token type - "+tok['token_type'])
				self.token_cache['access_token']=tok['access_token']
				if tok.has_key('refresh_token'):
					self.token_cache['refresh_token']=tok['refresh_token']
				self._auth_state=AS_ACC
			elif self._auth_state==AS_ACC:
				pass
		finally:
			self.auth_wait.release()

class OAuthViewWrapper(Dispatcher):
	"""Fulfills the Client role
	as defined in RFC 6749
	wraps a view based on a remote http data resource, implements OAuth2 protected
	access 
	"""
	def __init__(self,view,client_id,auth_uri,client_auth=None,token_ep_uri=None):
		self.view=view
		Dispatcher.__init__(self)
		view.d_s=self.dataDispatch
		self.auth_cache={} # key on state / cookie value
		self._http_client=ClientDispatcher()
		self.cb_map={}
	def getNewId(self):
		_hash=hashlib.new('sha1',str(id(self)))
		_hash.update(self.client_id)
		_hash.update(str(time.time()))
		_hash=_hash.hexdigest()
		self.auth_cache[_hash]={'auth_token':None,'access_token':None,'refresh_token':None,'timeout':0}
		return _hash
	def dispatch(self,event,cb=None):
		# catch non auth and do the dance or dispatch via view
		if event['req_headers'].has_key('cookie'):
			for cstr in event['req_headers']['cookie'].split(';'):
					k,v=cstr.split('=')
					if k.strip()=='OAID':
						if self.auth_cache.has_key(v):
							event['oauth_id']=v
							return self.view(event,cb=cb)
		elif event['req_query'].has_key('code'):
			# yay we have an auth token...
			# get an access token
			if self.auth_cache.has_key(event['req_query']['state']):
				self.auth_cache[event['req_query']['state']]['auth_token']=event['req_query']['code'][0]
			event['oauth_id']=event['req_query']['state']
			# should we catch it on callback this side??
			return self.view(event,cb=cb)
		elif event['req_query'].has_key('error'):
			# booo error response from auth server...
			# take us back to non-auth and deliver some kind of explaination page...
			del(self._auth_state[event['req_query']['state']])
			# TODO: return page with explaination based on error
			event['resp_code']=500
			raise StopIteration
		# no cookie assume unauthenticated....
		new_id=self.getNewId()
		event['resp_code']=302
		# TODO: sanely handle scopes
		event['resp_headers']['Location']='%s?response_type=code&%s' % (self.auth_uri,urllib.urlencode({'client_id':self.client_id,'state':new_id,'redirect_uri':httpGetUri(event)}))
		raise StopIteration
	def dataDispatch(self,event,cb=None):
		# override to dispatch data event to remote data source
		_auth_state=self.auth_cache[event['oauth_id']]
		if _auth_state['access_token'] and time.time()<_auth_state['timeout']:
			# valid access token available do request
			# should we form the request here or expect the data request to be passed into us??
			event['req_headers']['authorization']='bearer '+_auth_state['access_token']
			dispatchWithCallback(self._http_client,event,self.dataCb)
		elif _auth_state['refresh_token']:
			# refresh token available get new access token
			pass
		elif _auth_state['auth_token']:
			tok_req={"grant_type":"authorization_code","client_id":self.client_id,"redirect_uri":httpGetUri(event)}
			tok_request=HttpRequest('POST',self.token_ep)
			tok_request['req_headers']['content-type']="application/x-www-form-urlencoded"
			tok_request['request_body']=urllib.urlencode(tok_req)
			self.event_map[tok_request]=(event,cb)
			dispatchWithCallback(self._http_client,HttpRequest('POST',self.token_ep),self.authCb)
	def authCb(self,event,exc=None):
		# token endpoint response
		req,cb=self.event_map.pop(event)
		auth_state=self.auth_cache[req['req_headers']['oauth_id']]
		if event['resp_code']==200:
			# pull everything out of the token...
			tok=json.loads(event['resp_body'])
			if tok['token_type']!='bearer':
				# ouch... we don't implement anything else.. bailout and hope..
				raise TypeError("wrong token type - "+tok['token_type'])
			auth_state['access_token']=tok['access_token']
			if tok.has_key('refresh_token'):
				auth_state['refresh_token']=tok['refresh_token']
			# how to do original request here without triggering dispatch within callback lock??
			# probably never would as http client dispatcher is queue derived, but we really shouldn't assume
			# also do we need to have a dataCb or could we just pass on the original callback passed to us??
			dispatchWithCallback(self._http_client,req,cb=cb)
		else:
			# deal with error conditions here
			pass	
	def dataCb(self,event,exc=None):
		# TODO: catch errors and invalidate auth data
		pass
