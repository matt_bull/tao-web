
function fdataHandler(evt)	{
	//alert(evt.data);
	//console.debug(evt.data)
	//res=JSON.parse(evt.data);
	out=document.getElementById('user_info');
	if(evt.resp_type=='Error')	{
		out.innerHTML="Error - "+evt.msg;
	}else	{
		out.innerHTML="Response - <b>"+evt.uid+"</b> is "+evt.status;
	}
}

function connHandler(state,msg)	{
	conn_state=document.getElementById('conn_status');
	try	{
		conn_state.update(state,msg);
	} catch(TypeError)	{
		window.console.debug('Connection State Change - '+msg);
	}
}

function getUser()	{
	user_entry=document.getElementById("uid");
	fws.ws.send(JSON.stringify({'uid':user_entry.value,'action':'get'}));
}

function setStatus()	{
	user_entry=document.getElementById("uid");
	status_entry=document.getElementById("status");
	fws.ws.send(JSON.stringify({'uid':user_entry.value,'action':'set','status':status_entry.value}));	
}

var fws;
function setupJsonFinger(uri,proto)	{
	conn_state=document.getElementById('conn_status');
	conn_state.update=function(state,msg)	{
		switch(state)	{
			case CS_INP:
				this.textContent="Connecting...";
				break;
			case CS_CONN:
				this.textContent="Connected";
				break;
			case CS_DIS:
				this.textContent="Not Connected";
				break;
			case CS_ERR:
				this.textContent="ERROR";
				break;
		}
	}
	fws=new TaoWebSocket(uri,proto,fdataHandler,connHandler);
}

