from tao_web.widgets.websocket_view import WebSocketView
from tao_web import websocket
from tao_web.web_widget import View
from tao_core.sigIO import Protocol,SimpleBuffer
from tao_core.tdf_core import SimpleEvent
from tao_core.utils import RefList
import json

class JsonFingerWsView(WebSocketView):
	_embed_paths=['finger_client.js']
	_js_init='setupJsonFinger("%(doc_uri)s","json-finger")'
	_proto_name_='json-finger'
	def __init__(self,data_source,doc_uri=None,title=None,status_map={}):
		WebSocketView.__init__(self,data_source,doc_uri=doc_uri,title=title)
		self.status_map=status_map
		self.page=open('finger_client.htm','r').read()
		self.watchers={}
		#print self._embed_paths
	def getWebsocketDispatcher(self):
		return self.fingerDispatch
	def fingerDispatch(self,event,cb=None):
		# we can ignore cb as we never return true
		print "fingerDispatch",event.getDict()
		if self.status_map.has_key(event['uid']):
			if event.has_key('status'):
				self.status_map[event['uid']]=event['status']
				for dis in self.watchers[event['uid']]:
					dis(event)
			else:
				event['status']=self.status_map[event['uid']]
		else:
			raise KeyError("User Not Found")
		if event['dispatcher']:
			if self.watchers.has_key(event['uid']):
				self.watchers[event['uid']].append(event['dispatcher'])
			else:
				self.watchers[event['uid']]=[event['dispatcher']]
	def render(self,data,doc_uri=None):
		return self.page

class JsonFingerProtocol(Protocol):
	__in_buffer_class__=SimpleBuffer
	def onData(self):
		evt=json.loads(self.in_buffer.next())
		self.put(SimpleEvent('org.digitao.finger.Status',dispatcher=self.update,**evt),sync=True)
	def onSpawn(self,chan):
		self.put(SimpleEvent('org.digitao.finger.Status',dispatcher=self.update,uid='matt'))
	def update(self,evt):
		print ">>> Update",evt.getDict()
		d=evt.getDict()
		del(d['dispatcher'])
		self.send(json.dumps(d))
	def onCallback(self,event,exc=None):
		if exc:
			self.send(json.dumps({'resp_type':'Error','msg':repr(exc)}))
		else:
			print ">>> Response",event.getDict()
			self.send(json.dumps({'resp_type':'Status','status':event['status'],'uid':event['uid']}))

websocket.registerProtocol('json-finger',JsonFingerProtocol)

if __name__=="__main__":
	from tao_core import sigIO,sigIO_net
	import time
	# create our finger view
	def dataSrc(evt,cb=None):
		#evt['validated']=True
		pass
	view=JsonFingerWsView(dataSrc,doc_uri='http://localhost:8080/',status_map={'matt':'furtling'})
	io_main=sigIO.getIO()
	io_main.start()
	# create new 
	chan=io_main.newChannel(websocket.HttpWSProtocol,sigIO_net.TCPInetTransport,dispatcher=view,trans_args={'allow_reuse_addr':True})
	chan.listen(('0.0.0.0',8080))
	while 1:
		try:
			time.sleep(20)
		except KeyboardInterrupt:
			break
	chan.close()
	io_main.stop()

