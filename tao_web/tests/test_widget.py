if __name__=='__main__':
	import sys
	from tao_core import sigIO_net,sigIO
	from http import HttpServerProtocol
	import os,time
	from tao_web import web_widget
	io_main=sigIO.getIO()
	io_main.start()
	test_data={'foo':'bar','integer':2,'float':2.0,'list':range(5),
			'records':[]}
	for ind in range(20):
		test_data['records'].append(('row_%s' % ind,'test column %s' % ind,ind))
	def testDataSource(evt,cb=None):
		evt['result']=[test_data]
	t_v=web_widget.View(testDataSource,doc_uri='http://localhost:8080/test',title='test view')
	w=web_widget.XmlTemplateWidget(templ='tests/xml_test.xml')
	t_v.add(w)
	chan=io_main.newChannel(HttpServerProtocol,sigIO_net.TCPInetTransport,dispatcher=t_v)
	chan.listen((os.getenv('IP','0.0.0.0'),int(os.getenv('PORT',8080))))
	while 1:
		try:
			time.sleep(20)
		except KeyboardInterrupt:
			break
	chan.close()
	io_main.stop()
