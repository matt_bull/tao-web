from tao_core import sigIO,sigIO_net
from tao_web.web_widget import View,Widget
from tao_web.http import HttpServerProtocol
import time,os,urlparse

test_data={'foo':'bar','integer':2,'float':2.0,'list':range(5),'user':'matt@digitao.org',
		'records':[]}
for ind in range(20):
	if ind==10:
		ind=150
	test_data['records'].append((ind,'row_%s' % ind,'test column %s' % ind))

def testDataSource(evt,cb=None):
	evt['data_body']=test_data

def runTest(widget,uri=None):
	io_main=sigIO.getIO()
	io_main.start()
	if uri:
		loc=urlparse.urlsplit(uri).netloc
		try:
			host,port=loc.split(':')
		except IndexError:
			host=loc
			port=80
	else:
		host='0.0.0.0'
		port=8080
	if isinstance(widget,View):
		chan=io_main.newChannel(HttpServerProtocol,sigIO_net.TCPInetTransport,dispatcher=widget)
	else:
		view=View(testDataSource)
		view.add(widget)
		chan=io_main.newChannel(HttpServerProtocol,sigIO_net.TCPInetTransport,dispatcher=view,trans_args={'allow_reuse_addr':True})
	chan.listen((os.getenv('IP',host),int(os.getenv('PORT',port))))
	while 1:
		try:
			time.sleep(20)
		except KeyboardInterrupt:
			break
	chan.close()
	io_main.stop()

