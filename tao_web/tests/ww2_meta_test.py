#ww2 test
from tao_web import web_widget2

class TestWidget(web_widget2.Widget):
	_embed_paths=['test.js','test.css']
	_link_uris=['http://digitao.org/test.js']

class MetaTestWidget(TestWidget):
	_embed_paths=['test.js']
	_link_uris=['http://digitao.org/test.js']

class ClMethTest(object):
	@classmethod
	def getClass(cls):
		print cls

class SubCls(ClMethTest):
	pass

