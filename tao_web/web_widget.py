
# HTML Widget framework

# web widget framework, aimed at writing javascript heavy, html5 widgets for use in web pages.
# designed for use in a tao_web server.
# the difference from other frameworks is whilst this doesn't have a template language or even allow
# designers to work purely in html, it does enable the writing of reusable widgets (formed from javascript files
# css files and fragments of html) and handle all the messy path manipulation stuff (or having to have all your
# .js / .css files at a particular url, god forbid you ever need to change _that_) instead just import the widget
# and instanciate it just the same as any other gui toolkit (well almost)

# the difference between widgets and views....
# Widgets are static, and dumb, really just know how to render themselves into html
# Views are dispatchers which handle HttpRequestEvents and can render an entire document, or just a portion
# View requires a data source (a dispatcher) which requires events with a 'query' element (a tuple of <model>,<include>,<exclude>)
# and returns a list of results in 'results'

from urlparse import urlparse,urlsplit
import os,sys,json,imp
from tao_core.tdf_core import TrackDispatcher,dispatchWithCallback,SimpleEvent
from tao_web.http_dispatch import HttpPathDispatcher
from tao_web.http import httpMatchPath,httpGetUri,fileProducer
from xml.parsers import expat
import Queue,threading,thread,traceback

WIDGET_PATHS=[]
# append the widgets directory in the same directory as this module
WIDGET_PATHS.append(os.path.join(os.path.split(__file__)[0],'widgets'))

def mergeLinkUris(top,sub,base_uri):
	for item in sub:
		if hasattr(item,'getUri'):
			item=item.getUri(base_uri)
		if item not in top:
			top.append(item)

def mergeEmbedPaths(top,sub):
	for item in sub:
		if item not in top:
			top.append(item)

# 2 string subclasses for paths....
# manuipulate path depending on documents URI

def getHostRelativeUri(doc_uri,path):
	o=urlparse(doc_uri)
	if o.port:
		return o.scheme+'://'+o.hostname+':'+str(o.port)+'/'+path
	else:
		return o.scheme+'://'+o.hostname+'/'+path

class HostRelativeUri(str):
	def __init__(self,path):
		while path.startswith('/'):
			path=path[1:]
		str.__init__(self,path)
	def getUri(self,doc_uri):
		return getHostRelativeUri(doc_uri,self.__str__())

def getDocumentRelativeUri(doc_uri,path):
	o=urlparse(doc_uri)
	#return o.scheme+'://'+o.hostname+o.path.rsplit('/',1)[0]+'/'+path
	if o.port:
		return o.scheme+'://'+o.hostname+':'+str(o.port)+o.path+'/'+path
	else:
		return o.scheme+'://'+o.hostname+o.path+'/'+path

class DocumentRelativeUri(str):
	def __init__(self,path):
		while path.startswith('/'):
			path=path[1:]
		str.__init__(self,path)
	def getUri(self,doc_uri):
		return getDocumentRelativeUri(doc_uri,self.__str__())

def getWidgetRelativePath(mod_path,path):
	bpath=os.path.split(os.path.abspath(mod_path))[0]
	return os.path.join(bpath,path)

class WidgetRelativeResource(object):
	'''
	add to embed_paths in a widget to have the view pick up and add a handler for http serving of resource...
	i.e. to embed an image.....
	add WidgetRelativeResource(<relative path to image>) to _embed_paths
	in render.... "<img src=%s />" % self._embed_paths[<index of image resource>].getUri(doc_uri)
	'''
	def __init__(self,path,tag='',mime_type=None):
		super(WidgetRelativeResource,self).__init__()
		self.path=path
		self._id=tag
		self.basepath=None
		self.mime_type=mime_type
	def setBasepath(self,bpath):
		#print "WidgetRelativeResource.setBasepath",self.path,bpath
		self.basepath=bpath
	def getUri(self,doc_uri):
		return '/'.join([doc_uri,self.path])
	def __repr__(self):
		return "<WidgetRelativeResource at %s for path %s>" % (id(self),os.path.join(self.basepath,self.path))
	def __str__(self):
		if self.basepath:
			return os.path.abspath(os.path.join(self.basepath,self.path))
		else:
			raise TypeError('You must set basepath with setBasepath before you can retrieve actual path')
	#def __cmp__(self,ot):
	#	print "cmp with -",ot
	#	try:
	#		if self.basepath==ot.basepath and self.path==ot.path:
	#			return 0
	#	except:
	#		pass
	#	return -1

class _WidgetMeta_(type):
	def __init__(cls,name,bases,dct):
		b_mod_path=os.path.split(os.path.abspath(sys.modules[cls.__module__].__file__))[0]
		emb_paths=[]
		#print "paths in class",cls,cls._embed_paths
		for path in cls._embed_paths:
			if type(path)==WidgetRelativeResource:
				#print "Setting Basepath ->",cls,b_mod_path
				path.setBasepath(b_mod_path)
				emb_paths.append(path)
			else:
				emb_paths.append(os.path.join(b_mod_path,path))
		cls._embed_paths=emb_paths[:]
		# catch any WidgetRelativeResource and set basepath
		#for uri in cls._link_uris:
		#	if type(uri)==WidgetRelativeResource:
		#		uri.setBasepath(b_mod_path)
		for base in bases:
			#print cls,"=>",base
			cls.__updatelinks__(cls,base)
		#print cls,cls._embed_paths
		return type.__init__(cls,name,bases,dct)
	@classmethod
	def __updatelinks__(meta,cls,base):
		try:
			#print base._embed_paths
			for path in base._embed_paths:
				if path not in cls._embed_paths:
					#cls._embed_paths.insert(0,path)
					cls._embed_paths.append(path)
			for uri in base._link_uris:
				cls._link_uris.append(uri)
		except AttributeError:
			pass

def getCssConstraint(hw,const):
	css=''
	if const[0]:
		css="%smin-%s:%spx;" % (css,hw,const[0])
	if const[1]:
		css="%smax-%s:%spx;" % (css,hw,const[1])
	return css

class Widget:
	__metaclass__=_WidgetMeta_
	_embed_paths=[]
	_link_uris=[]
	_js_init=""
	#_post_js_init=None
	def __init__(self,title=None,w_id=None):
		self.title=title
		if w_id:
			self.wid=w_id
		else:
			self.wid=self.__class__.__name__+str(id(self))
		#if setting height or width constraints they must be min,max in pixels, None means do not set...
		self.width_constraint=(None,None)
		self.height_constraint=(None,None)
		#self.js_init_string=self._js_init % {'id':self.wid,'doc_uri':doc_uri,'title':title}
		self.bound_key=None
	def bind(self,key):
		self.bound_key=key
	def getResources(self,doc_uri):
		# return (js_init,links,embeds)
		embeds=[]
		links=[]
		#path=sys.modules[self.__module__].__file__
		for emb in self._embed_paths:
			if emb not in embeds:
				#print self,emb
				embeds.append(emb)
		for link in self._link_uris:
			if type(link)!=str:
				link=link.getUri(doc_uri)
			links.append(link)
		#print repr(self),'====>',embeds
		return (links,embeds)
	def getJsInit(self,doc_uri=None):
		# be careful if you override this, you should be able to do enough with just _js_init and getInitArgs()
		if self._js_init:
			# override getInitArgs to use other args these are guarenteed
			init_args={'id':self.wid,'doc_uri':doc_uri,'title':self.title}
			init_args.update(self.getInitArgs(doc_uri=doc_uri))
			js_init=[("window.__tww_%(id)s__="+self._js_init) % init_args]
		else:
			js_init=[]
		return js_init
	def getInitArgs(self,doc_uri=None):
		return {}
	def findResource(self,tag):
		for res in self._embed_paths:
			if type(res)==WidgetRelativeResource:
				if res._id==tag:
					return res
	def findWidgetForId(self,w_id):
		if self.wid==w_id:
			return self
	def add(self,obj):
		pass
	def render(self,data,doc_uri=''):
		# override to return html string for widget, based on doc_uri and data, if self.bound_key is not none
		# you should take that as the key to select from data
		text=["<div id='%s' style='%s'>base widget class - %s" % (self.wid,getCssConstraint('height',self.height_constraint)+getCssConstraint('width',self.width_constraint),self.title)]
		cnt=1
		if self.bound_key:
			text.append('\n<b>Bound To Key</b> - '+self.bound_key)
		for key in data.keys():
			text.append('\n<b>Data</b>')
			text.append('\n<br>&nbsp;&nbsp;<b>'+key+'</b> - '+str(res[key]))
		text.append('\n</div>\n')
		return text

class RowWidget(Widget):
	'''_HEAD_TMPL and _FOOT_TMPL class constants should be string sub format strings using the toplevel data,
	if required and renderRow should be overidden to return a string for a row representing record'''
	_HEAD_TEMPL=''
	_FOOT_TEMPL=''
	def render(self,data,doc_uri=''):
		yield self._HEAD_TEMPL % data
		if self.bound_key:
			for rec in data[self.bound_key]:
				yield self.renderRow(rec)
		elif data.has_key('records'):
			for rec in data['records']:
				yield self.renderRow(rec)
		yield self._FOOT_TEMPL % data
	def renderRow(self,record):
		raise NotImplemented('renderRow not implented in RowWidget subclass <%s>' % self)

class ContainerWidget(Widget):
	# static widget tree, no path dispatch required (ie full html returned, early bound)
	def __init__(self,child_widgets=[],title=None,w_id=None):
		Widget.__init__(self,title=title,w_id=w_id)
		self.children=child_widgets[:]
	def getResources(self,doc_uri):
		# return (links,embeds)
		embeds=self._embed_paths[:]
		links=[]
		#path=sys.modules[self.__module__].__file__
		for link in self._link_uris:
			links.append(link)
		for child in self.children:
			c_links,c_embeds=child.getResources(doc_uri)
			#print '+++',embeds,c_embeds
			mergeEmbedPaths(embeds,c_embeds)
			mergeLinkUris(links,c_links,doc_uri)
		#print '>>>',embeds,'from',self.children
		return (links,embeds)
	def getJsInit(self,doc_uri=None):
		#print 'ContainerWidget.getJsInit',self.children
		js_init_strings=Widget.getJsInit(self,doc_uri=doc_uri)
		# we should be initialised before our children.. they may depend on us
		for child in self.children:
			#print 'ContainerWidget.getJsInit',child
			try:
				js_init_strings.extend(child.getJsInit(doc_uri=doc_uri))
			except Exception,exc:
				print exc
				traceback.print_exc()
		#print js_init_strings
		return js_init_strings
	def findWidgetForId(self,w_id):
		if self.wid==w_id:
			return self
		else:
			for child in self.children:
				trg=child.findWidgetForId(w_id)
				if trg:
					return trg
	def add(self,widget,index=None):
		#print "ContainerWidget.add",widget,'->',self
		if isinstance(widget,View):
			# switch off doc_reneder as this is obviously not the top level
			widget.doc_renderer=False
		if index==None:
			self.children.append(widget)
		else:
			self.children.insert(index,widget)
	def remove(self,child):
		self.children.remove(child)
	def render(self,data,doc_uri=''):
		# default just renders each child widget...
		for child in self.children:
			yield child.render(data,doc_uri=doc_uri)

class View(TrackDispatcher,ContainerWidget):
	__metaclass__=_WidgetMeta_
	_embed_paths=[]
	''' the difference between a widget and a view....
	only a view can be a dispatcher (although it doesn't have to be) and must be able to render a full page...
	although as a subclass of ContainerWidget if you don't pass in a dispatcher as data_source it can be treated as such
	hence almost anything which could be a standalone page should be subclassed from view, so it can be used standalone if neccesary
	'''
	def __init__(self,data_source=None,doc_uri=None,title=None):
		TrackDispatcher.__init__(self,dispatcher=data_source)
		ContainerWidget.__init__(self,title=title)
		self.doc_uri=doc_uri
		#self.d_s=data_source
		self.doc_renderer=True
		self.resource_map={}
	def getJsInit(self,doc_uri=None):
		if not doc_uri:
			doc_uri=self.doc_uri
		return ContainerWidget.getJsInit(self,doc_uri=doc_uri)
	def dispatch(self,evt,cb=None):
		#print 'uris.....',evt['req_path'],self.resource_map
		if self.resource_map.has_key(evt['req_path']):
			# this is one of our resources.....
			try:
				#print "!!!",str(self.resource_map[evt['req_path']])
				evt['resp_body']=fileProducer(str(self.resource_map[evt['req_path']]))
				#print '!!!',self.resource_map[evt['req_path']].mime_type
				if self.resource_map[evt['req_path']].mime_type:
					evt['resp_type']=self.resource_map[evt['req_path']].mime_type
			except IOError:
				evt['resp_code']=404
				return
			evt['resp_code']=200
			#print '!!!!',len(evt['resp_body'])
		elif (not self.doc_uri) or evt['req_path']==urlsplit(self.doc_uri).path:
			if self._dispatcher:
				d_evt=self.adaptEvent(evt)
				return TrackDispatcher.dispatch(self,d_evt,cb=cb)
				#return dispatchWithCallback(self._dispatcher,d_evt,self.finish)
			else:
				evt['result']=[]
				self.cb_map[evt]=cb
				self.finish(evt)
		else:
			evt['resp_code']=404
	def getResources(self,doc_uri):
		# get a fully merged version of links and paths....
		res=ContainerWidget.getResources(self,doc_uri)
		#print "View.getResources->",res
		for p in res[1][:]:
			#print repr(p)
			if type(p)==WidgetRelativeResource:
				if not self.resource_map.has_key(urlsplit(p.getUri(doc_uri)).path):
					self.resource_map[urlsplit(p.getUri(doc_uri)).path]=p
				res[1].remove(p)
		return res
	def finish(self,evt,exc=None):
		#print 'View.finish',evt,exc
		_accept_type={}
		for mime_type in evt['req_headers']['accept'].split(','):
			#print mime_type
			if ';' in mime_type:
				try:
					mime_type,qual=mime_type.split(';')
					qual=float(qual.split('=')[1])
				except:
					continue
			else:
				qual=1
			if _accept_type.has_key(qual):
				_accept_type[qual].append(mime_type)
			else:
				_accept_type[qual]=[mime_type]
		accept_type=[]
		for qual in sorted(_accept_type.keys()):
			accept_type.extend(_accept_type[qual])
		evt,body=self.renderEvent(evt,mime_types=accept_type)
		if exc and exc!=StopIteration:
			evt['resp_code']=500
		else:
			#print 'View.finish>>>>',evt['resp_type'],body
			if self.doc_uri:
				doc_uri=self.doc_uri
			else:
				doc_uri=httpGetUri(evt)
			links,embeds=self.getResources(doc_uri)
			#print 'embeds ->',embeds,self.children
			js_init=self.getJsInit(doc_uri=doc_uri)
			evt['resp_headers']['Location']=doc_uri
			#print ">>> JS Init >>>",repr(js_init)
			if evt['resp_type'] not in ('text/html','application/xhtml+xml'):
				# have to catch this before looking at self.doc_renderer
				# as that will only be untrue if we are inside a container or wrapper...
				#evt['js_init']=js_init
				evt['resp_body']=body
			elif self.doc_renderer:
				evt['resp_code']=200
				#print '::embeds ->',embeds
				evt['resp_body']=HtmlDocument(links,embeds,js_init,body,title=self.title)
			elif isPartialReq(evt):
				#print '>>>> Partial Request!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
				evt['resp_code']=200
				evt['resp_body']=PartialDocument(js_init,body)
				evt['resp_headers']['Document-Title']=self.title
			else:
				# we may be inside a wrapper....
				evt['js_init']=js_init
				evt['resp_body']=body
		TrackDispatcher.finish(self,evt,exc=exc)
	def adaptEvent(self,evt):
		# override to adapt event for subsequent dispatchers, or create and map a new event as in EventAdapter
		# default adapts event for data_model style DataStore
		if evt['req_type']=='GET':
			if evt['req_query'] and evt['req_query'].has_key('id'):
				evt['data_op']=DAT_READ
				if evt['req_query'].has_key('collection'):
					evt['data_collection']=evt['req_query']['collection']
				else:
					evt['data_collection']='_default_'
				evt['data_id']=evt['req_query']['id']
				evt['data_body']={}
			elif evt['req_query']:
				try:
					coll=evt['req_query'].pop('colection')
				except KeyError:
					coll='_default_'
				key=evt['req_query'].keys()[0]
				evt['data_query']=(key,evt['req_query'][key],[])
				evt['data_collection']=coll
				evt['data_inc_records']=True
				evt['data_result']=[]
			else:
				evt['data_query']=('id',[],[])
		return evt
	def renderEvent(self,evt,mime_types=['text/html']):
		# the other end of adaptEvent above, if new event was returned there, get the original and transfer what was needed
		# render and return tuple of HttpEvent and rendered body...
		if self.doc_uri:
			doc_uri=self.doc_uri
		else:
			doc_uri=httpGetUri(evt)
		#print 'rendering',evt
		if 'text/html' in mime_types or '*/*' in mime_types:
			evt['resp_type']='text/html'
		else:
			# should we barf here??
			evt['resp_type']=mime_types[0]
		if evt.has_key('data_result'):
			return (evt,self.render({'query_result':evt['data_result']},doc_uri=doc_uri))
		elif evt.has_key('data_body'):
			return (evt,self.render(evt['data_body'],doc_uri=doc_uri))
		else:
			return (evt,self.render({},doc_uri=doc_uri))
	def render(self,data,doc_uri=None):
		if self.children:
			return ContainerWidget.render(self,data,doc_uri=doc_uri)
		else:
			return Widget.render(self,data,doc_uri=doc_uri)

class RestView(View):
	def __init__(self,collection,**kwargs):
		View.__init__(self,**kwargs)
		self.collection=collection
	def adaptEvent(self,evt):
		if evt['req_type']=='GET':
			if evt['disp_path'][-1]==self.collection:
				# we are at the end of the disp_path get the collection
				evt['data_query']=('id',[],[])
				evt['data_collection']=self.collection
				evt['data_inc_records']=True
				evt['data_result']=[]
			elif len(evt['disp_path'])>1 and evt['disp_path'][-2]==self.collection:
				# this is a call for a member of the collection
				evt['data_op']=DAT_READ
				evt['data_collection']=self.collection
				evt['data_id']=evt['disp_path'][-1]
				evt['data_body']={}
		elif evt['req_type']=='POST':
			# create new item in collection
			if evt['disp_path'][-1]!=self.collection:
				raise KeyError('incorrect collection name')
			evt['data_op']=DAT_CREATE
			evt['data_collection']=self.collection
			if evt['req_query'].has_key('id'):
				evt['data_id']=evt['req_query'].pop('id')
			else:
				evt['data_id']=None
			evt['data_body']=evt['req_query']
		elif evt['req_type']=='PUT':
			# update item in collection
			if evt['disp_path'][-2]!=self.collection:
				raise KeyError('incorrect collection name')
			evt['data_op']=DAT_UPDATE
			evt['data_collection']=self.collection
			evt['data_id']=evt['disp_path'][-1]
			evt['data_body']=evt['req_query']
		elif evt['req_type']=='DELETE':
			evt['data_op']=DAT_DELETE
			evt['data_collection']=self.collection
			evt['data_id']=evt['req_query']['id']
		return evt
	def renderEvent(self,evt):
		# todo: find a way of distiguishing the xhr json requests from page requests
		# then render those directly
		if self.doc_uri:
			doc_uri=self.doc_uri
		else:
			doc_uri=httpGetUri(evt)
		if evt.has_key('data_result'):
			return (evt,self.renderCollection(evt['data_result'],doc_uri=doc_uri))
		else:
			return (evt,self.renderItem(evt['data_body'],doc_uri=doc_uri))
	def renderCollection(self,data,doc_uri=None):
		# render list of collection items
		pass
	def renderItem(self,data,doc_uri=None):
		# render individual record from collection
		pass

'''class XhrView(View):
	def dispatch(self,event,cb=None):
		for mime_type in evt['req_headers']['accept'].split(';'):
			if mime_type=='*/*':
				break
			if mime_type in ('application/json','application/xml','text/xml'):
				# this is an xhr request set accpet_type in event
				event['accept_type']=mime_type
		if evt['req_headers']['content-type']=='application/json':
			# we need to parse this here into a query
			pass
		return View.dispatch(self,event,cb=cb)
	def finish(self,evt,exc=None):
		if event.has_key('accept_type'):
			 #this is an xhr event with a specific resp type...
			evt['resp_headers']['content-type'],evt['resp_body']=self.renderXhr(evt['data_action'],evt['result'],resp_format=evt['req_headers']['Accept'])
			evt['resp_code']=200
		else:
			# ordianry page request, handle it as view
			View.finish(self,evt,exc=exc)
	def renderXhr(self,action,result,resp_format='*/*'):
		# override to return tuple of mimetype, response body.
		raise NotImplementedError('renderXhr not implemented in XhrView')
'''
class ViewWrapper(View):
	"""Wrap a view for extra functionality"""
	__metaclass__=_WidgetMeta_
	def __init__(self,view,title=None):
		View.__init__(self,view,doc_uri=self.cview.doc_uri,title=title)
		self._dispatcher.doc_renderer=False
	def dispatch(self,event,cb=None):
		event=self.parseEvent(event)
		return TrackDispatcher.dispatch(self,event,cb=cb)
	def parseEvent(self,event):
		# override to do something with event on the way downstream
		return event
	def getResources(self,doc_uri):
		# return (links,embeds)
		embeds=self._embed_paths[:]
		links=[]
		#path=sys.modules[self.__module__].__file__
		for link in self._link_uris:
			links.append(link)
		c_links,c_embeds=self.cview.getResources(doc_uri)
		#print '+++',embeds,c_embeds
		mergeEmbedPaths(embeds,c_embeds)
		mergeLinkUris(links,c_links,doc_uri)
		#print '>>>',embeds
		return (links,embeds)
	def finish(self,evt,exc=None):
		if self.doc_renderer:
			js_init=self.getJsInit()
			if evt.has_key('js_init'):
				js_init.extend(evt['js_init'])
			links,embeds=self.getResources(self.doc_uri)
			evt,body=self.renderEvent(evt)
			if evt['resp_code']==0:
				evt['resp_code']=200
				evt['resp_body']=HtmlDocument(links,embeds,js_init,self.render(evt['result']),title=self.cview.title)
			elif evt['resp_code']==200:
				evt['resp_body']=HtmlDocument(links,embeds,js_init,self.render(evt['result'],body=evt['resp_body']),title=self.cview.title)			
		TrackDispatcher.finish(self,evt,exc=exc)
	def render(self,data,doc_uri=None,body=''):
		# override to output html around body.
		pass

class ViewContainer(HttpPathDispatcher,TrackDispatcher,ContainerWidget):
	__metaclass__=_WidgetMeta_
	# container for other views usually top level navigation item (late bound, loads content dynamically via XmlHttp)
	# can also be used with wrap all views in an app (navigable by path) in html (ie login)
	def __init__(self,doc_uri=None,title=''):
		self.doc_uri=doc_uri
		HttpPathDispatcher.__init__(self)
		TrackDispatcher.__init__(self)
		ContainerWidget.__init__(self,title=title)
		self.title_map={}
	def getJsInit(self,doc_uri=None):
		return Widget.getJsInit(self,doc_uri=doc_uri)
	def addView(self,view,title):
		# set doc_renderer to false so we don't get an html doc back...
		view.doc_renderer=False
		path=title.replace(' ','_').lower()
		self.registerDispatcher(path,view)
		# manipulate the doc_uri...
		if self.doc_uri:
			view.doc_uri=self.doc_uri+'/'+path
		self.title_map[title]=path
		view.title=title
		if not self.default:
			self.default=path
	def add(self,view,title,index=None):
		self.addView(view,title)
		ContainerWidget.add(self,view,index=index)
	def remove(self,view):
		key=self.title_map[view.title]
		self.removeDispatcher(key,view)
		ContainerWidget.remove(self,view)
		del(self.title_map[view.title])
		if self.default==key and self.dispatch_map:
			self.default=self.dispatch_map.keys()[0]
	def dispatch(self,evt,cb=None):
		#print "ViewContainer.dispatch -->>",httpGetUri(evt),evt,cb
		#print "ViewContainer dispatch_map -->>",evt['resp_code'],self.dispatch_map,evt['req_path']
		tab_key=evt['req_path'].rsplit('/',1)[1]
		if tab_key in self.dispatch_map.keys():
			evt['tab_key']=tab_key
			self.cb_map[evt]=cb
			return dispatchWithCallback(self.dispatch_map[tab_key],evt,self.finish)
		else:
			#print "doing default %s" % self.default
			evt['tab_key']=self.default
			if evt['req_path'].endswith('/'):
				evt['req_path']=evt['req_path']+self.default
			else:
				evt['req_path']=evt['req_path']+'/'+self.default
			self.cb_map[evt]=cb
			return dispatchWithCallback(self.dispatch_map[self.default],evt,self.finish)
	def finish(self,evt,exc=None):
		if not exc:
			if self.doc_uri:
				doc_uri=self.doc_uri
			else:
				doc_uri=httpGetUri(evt).rsplit('/',1)[0]
			#print "finish in ViewContainer -->>",evt['resp_code'],evt['resp_body']
			if evt.has_key('tab_key'):
				title=self.dispatch_map[evt['tab_key']].title
			else:
				title=self.title
			if not evt['resp_code']:
				js_init=self.getJsInit(doc_uri=doc_uri)
				js_init.extend(evt['js_init'])
				#print js_init
				links,embeds=self.getResources(doc_uri)
				evt['resp_code']=200
				evt['resp_body']=HtmlDocument(links,embeds,js_init,self.render(evt['tab_key'],evt['result'],body=evt['resp_body'],doc_uri=doc_uri),title=self.title+' - '+title)
		TrackDispatcher.finish(self,evt,exc=exc)
	def render(self,child_path,data,doc_uri=None,body=''):
		# override to output html around body.
		pass

def isPartialReq(evt):
	# todo: get is a request is for partial html or full document...
	#print '>>>',evt['req_headers']
	if evt['req_headers'].has_key('dtao-partial-request') or evt['req_headers'].has_key('x-partial-content'):
		return True
	elif evt['req_headers'].has_key('accepted') and evt['req_headers']['accepted']=='text/html-fragment':
		return True
	return False

def PartialDocument(js_init,body):
	yield "["+json.dumps(js_init)+',"'
	for tl in recursiveIter(body):
		yield tl.replace('\n','').replace('"','\"')
	yield '"]'
	

def HtmlDocument(links,embeds,js_init,body,title=None):
	#print embeds
	yield '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN http://www.w3.org/TR/html4/frameset.dtd">\n<html>\n<head>\n'
	if title:
		yield '<title>'+title+'</title>\n'
	if js_init:
		# essential stuff like init function, post init array and inheritsFrom()
		yield "<script type='application/javascript'>\nvar post_init=new Array();\nvar inheritsFrom=function(c,p)\t{\n\tc.prototype=Object.create(p.prototype);\n}\nvar bind_registry={};\nwindow.onload=function()\t{\n\t"+"\n\t".join(js_init)+"\n\tfor(i=0;i<post_init.length;i++){\n\t\tpost_init[i]();\n\t}\n}\n</script>\n"
		#yield "<script type='text/javascript'>\nwindow.onload=function()\t{\n"
		#for init in js_init:
		#	yield "\teval('"+init+"');\n"
		#yield "}\n</script>"
	for link in links:
		if link.endswith('.css'):
			yield '<link rel="stylesheet" href="%s" type="text/css" />\n' % link
		elif link.endswith('.js'):
			yield '<script type="application/javascript" src="%s"></script>\n' % link
	css_str=""
	js_str=""
	for fpath in embeds:
		#print "loading >>",fpath
		if type(fpath) in (str,unicode):
			if fpath.endswith('.css'):
				# stylesheet... use a style tag...
				try:
					css_str=css_str+'\n'+open(fpath).read()
				except:
					print "[Resource Error] error loading resource %s" % fpath
			elif fpath.endswith('.js'):
				#print "loading >>",fpath
				try:
					js_str=js_str+'\n'+open(fpath).read()
				except:
					print "[Resource Error] error loading resource %s" % fpath
	if css_str:
		# stylesheet... use a style tag...
		#print css_str
		yield '<style type="text/css">'+css_str+'\n</style>\n'
	if js_str:
		yield '<script type="application/javascript">'+js_str+'\n</script>\n'
	yield "</head>\n<body style='margin:0px;padding:0px;'>\n"
	for st in recursiveIter(body):
		#print 'BODY>>>>',repr(st)
		yield st
	yield '\n</body>\n</html>'

class SimpleTemplateWidget(Widget):
	def __init__(self,templ_path,title=None):
		Widget.__init__(self,title=title)
		if templ_path.startswith('/'):
			self.templ_str=open(templ_path,'r').read()
		else:
			self.templ_str=open(getWidgetRelativePath(sys.modules[self.__module__].__file__,templ_path),'r').read()
	def render(self,data,doc_uri=''):
		doc_data={'id':self.wid,'title':self.title,'doc_uri':doc_uri}
		doc_data.update(data[0])
		return [self.templ_str % doc_data]

def getWidget(widget_name,path=[]):
	#print widget_name
	#for pth in path:
	#	#print os.path.abspath(pth)
	if '.' in widget_name:
		mod_name,cls_name=widget_name.split('.',1)
		if not path:
			meta=imp.find_module(mod_name,WIDGET_PATHS)
		else:
			meta=imp.find_module(mod_name,path)
		# check if module is already imported...
		for mod_id in sys.modules.keys():
			if mod_id.endswith(mod_name) and hasattr(sys.modules[mod_id],'__file__'):
				#print 'Possible >>>',mod_name,mod_id,sys.modules[mod_id],meta[1]
				if sys.modules[mod_id].__file__==meta[1]:
					#print '!!!Found >>',mod_name,mod_id,sys.modules[mod_id],meta[1]
					return getattr(sys.modules[mod_id],cls_name)
		if meta[0]:
			widget_mod=imp.load_module(mod_name,*meta)
			return getattr(widget_mod,cls_name)
		else:
			return getWidget(cls_name,meta[1])
	else:
		raise ValueError('no module name specified in widget name')

class XmlTag:
	# we never have width or height contraints but these are still required for Box and its derivatives
	width_constraint=(None,None)
	height_constraint=(None,None)
	def __init__(self,tag,attr):
		self.tag=tag
		if attr.has_key('bind'):
			self.bound_key=attr['bind']
		else:
			self.bound_key=None
		self.attr=attr
		self._text=''
		self._cont=[]
	def add(self,obj):
		self._cont.append(obj)
	def getResources(self,doc_uri):
		# return (links,embeds)
		links=[]
		embeds=[]
		for child in self._cont:
			if isinstance(child,Widget) or isinstance(child,XmlTag):
				c_links,c_embeds=child.getResources(doc_uri)
				mergeEmbedPaths(embeds,c_embeds)
				mergeLinkUris(links,c_links,doc_uri)
		return (links,embeds)
	def getJsInit(self,doc_uri=None):
		return []
	def findWidgetForId(self,w_id):
		for child in self._cont:
			trg=child.findWidgetForId(w_id)
			if trg:
				return trg
	def getText(self):
		return self._text
	def render(self,data,doc_uri=None):
		#print 'XmlTag.render ->',self.tag,self._cont
		attr=[]
		for key in self.attr.keys():
			if type(self.attr[key])==unicode:
				if u"'" in self.attr[key]:
					attr.append('%s="%s"' % (key,self.attr[key]))
				else:
					attr.append("%s='%s'" % (key,self.attr[key]))
			else:
				attr.append('%s=%s' % (key,self.attr[key]))
		t_st_str='<%s %s' % (self.tag,' '.join(attr))
		if self.tag.lower() in ('area','base','br','col','command','embed','hr','img','input','keygen','link','meta','param','source','track','wbr'):
			# these are html tags that are singles....
			yield t_st_str.strip()+">"
			return
		elif self._cont or self.tag.lower() in ('div','span'):
			# we catch div and span as they are commonly empty but illegal as a sigle tag in html
			yield t_st_str.strip()+">"
		else:
			yield t_st_str.strip()+" />"
			return
		#print data,self.bound_key
		try:
			yield data[self.bound_key]
		except:
			pass
		for elem in self._cont:
			if type(elem) in (str,unicode):
				yield elem
			else:
				yield elem.render(data,doc_uri=doc_uri)
		yield '</%s>' % self.tag

class ImageWidget(Widget):
	def __init__(self,path,base_path,attr={}):
		Widget.__init__(self)
		self._embed_paths=self._embed_paths[:]
		self._embed_paths.append(WidgetRelativeResource(path))
		self._embed_paths[0].setBasepath(base_path)
		self.attr=attr
		try:
			self.attr.pop('src')
		except KeyError:
			pass
	def render(self,data,doc_uri=''):
		tag_str='<img src="%s"' % self._embed_paths[0].getUri(doc_uri)
		for key,value in self.attr.items():
			tag_str='%s %s=%s' % (tag_str,key,repr(str(value)))
		tag_str=tag_str+'>'
		return tag_str

class XmlTemplateWidget(ContainerWidget):
	# TODO: this appears to have a problem with empty divs.... it renders the following tags into them instead of rendering them empty
	def __init__(self,title=None,templ='tmpl.xml',widget_path=['./widgets']):
		ContainerWidget.__init__(self,title=title)
		self._stack=[]
		self.widget_path=[]
		for path in widget_path:
			self.widget_path.append(os.path.abspath(path))
		self.widget_path.extend(WIDGET_PATHS) # add in main tao_web widgets
		self.templ_dir=os.path.split(os.path.abspath(templ))[0]
		self._embed_paths=self._embed_paths[:]
		self.parser=expat.ParserCreate()
		self.parser.StartElementHandler=self._stelem
		self.parser.EndElementHandler=self._endelem
		self.parser.CharacterDataHandler=self._chelem
		self.in_template=False
		self.parser.Parse(open(os.path.abspath(templ),'r').read())
	def _stelem(self,tag,attr):
		#print '>>>',tag
		# create widget....
		if tag=='template':
			self._stack.append(self)
			self.in_template=True
			return
		elif not self.in_template:
			print "[ERR] ingnoring tag %s outside of template tags" % tag
			return
		if tag.startswith('widget:'):
			#print tag.split(':'),self.widget_path
			w_c=getWidget(tag.split(':',1)[1],path=self.widget_path)
			try:
				bind=attr.pop('bind')
			except:
				bind=None
			if attr.has_key('actions'):
				# special attribute parse into dict of <title>:<javascript function>
				actions=[]
				for pstr in attr['actions'].split(','):
					title,action=pstr.split(':')
					actions.append((action,title))
				attr['actions']=actions
			w_i=w_c(**attr)
			if bind:
				w_i.bind(bind)
			self._stack.append(w_i)
			#print self._stack
		elif tag=='embed':
			#print os.path.join(self.templ_dir,attr['path'])
			self._embed_paths.append(os.path.abspath(os.path.join(self.templ_dir,attr['path'])))
		elif tag=='img':
			#print attr
			if attr.has_key('src'):
				if attr['src'].startswith('http://') or attr['src'].startswith('https://'):
					# non relative uri.. bypass
					self._stack.append(XmlTag(tag,attr))
				else:
					#relative uri create an ImageWidget
					self._stack.append(ImageWidget(attr['src'],self.templ_dir,attr=attr))
			else:
				self._stack.append(XmlTag(tag,attr))
		else:
			self._stack.append(XmlTag(tag,attr))
	def _endelem(self,tag):
		#print '<<<',tag,self._stack
		if tag=='embed':
			#ignore these
			return
		#if self._stack and self._stack[-1].
		if len(self._stack)==1 and tag=='template':
			self._stack.pop()
			self.in_template=False
		elif not self._stack:
			print 'Unexpected tag outside template tags <%s>' % (tag)
		elif isinstance(self._stack[-2],Widget) or isinstance(self._stack[-2],XmlTag):
			self._stack[-2].add(self._stack.pop())
		else:
			print '??',self._stack[-2]
		#print '<<',self.children
	def _chelem(self,text):
		if text.strip() and self._stack and isinstance(self._stack[-1],XmlTag):
			self._stack[-1].add(text)
	def getResources(self,doc_uri):
		# return (links,embeds)
		embeds=self._embed_paths[:]
		links=[]
		#path=sys.modules[self.__module__].__file__
		for link in self._link_uris:
			links.append(link)
		for child in self.children:
			c_links,c_embeds=child.getResources(doc_uri)
			#print 't+++',embeds,c_embeds
			mergeEmbedPaths(embeds,c_embeds)
			mergeLinkUris(links,c_links,doc_uri)
		#print 't>>>',embeds
		return (links,embeds)
	def render(self,data,doc_uri=''):
		for elem in self.children:
			print "render >>>>",elem
			# this should be either a string or a widget...
			#print type(elem),data
			yield elem.render(data,doc_uri=doc_uri)

def recursiveIter(elements):
	#print '>>>>',type(elements),elements
	if type(elements) in (str,unicode):
		yield elements
	else:
		try:
			for element in elements:
				if type(element)==str:
					yield element
				else:
					try:
						child=recursiveIter(element)
					except Exception,exc:
						sys.stderr.write('recursiveIter - skipping element %s\n\ton error %s\n' % (repr(element),exc))
					for c_elem in child:
						yield c_elem
		except TypeError,exc:
			print "error rendering -->>",elements
			traceback.print_exc()

if __name__=='__main__':
	from tao_core import sigIO_net,sigIO
	from tao_web.web_widget import *
	from http import HttpServerProtocol
	import os,time
	io_main=sigIO.getIO()
	io_main.start()
	test_data={'foo':'bar','integer':2,'float':2.0,'list':range(5),
			'records':[]}
	for ind in range(20):
		test_data['records'].append(('row_%s' % ind,'test column %s' % ind,ind))
	def testDataSource(evt,cb=None):
		evt['data_body']=test_data
	# test widget
	class TestWidget(Widget):
		_embed_paths=['tests/test.js',WidgetRelativeResource('tests/ok.png',mime_type='image/png')]
		_link_uris=[]
		_js_init="init_test('%(id)s');"
		def render(self,res,doc_uri=''):
			return "<img src=%s />" % self._embed_paths[1].getUri(doc_uri)
	t_v=View(testDataSource,doc_uri='http://localhost:8080/test',title='test view')
	#t_w=SimpleTemplateWidget('tests/test.htt',title="template widget test")
	#t_v.add(t_w)
	#w=TestWidget()
	w=XmlTemplateWidget(templ='tests/xml_test.xml')
	t_v.add(w)
	chan=io_main.newChannel(HttpServerProtocol,sigIO_net.TCPInetTransport,dispatcher=t_v)
	chan.listen((os.getenv('IP','0.0.0.0'),int(os.getenv('PORT',8080))))
	while 1:
		try:
			time.sleep(20)
		except KeyboardInterrupt:
			break
	chan.close()
	io_main.stop()
