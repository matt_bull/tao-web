from http import HttpServerProtocol,httpDate,HttpProtocol
from tao_core.sigIO import SimpleBuffer,SimpleProducer
from tao_core.tdf_core import Dispatcher,TrackDispatcher
from tao_web.http import ST_IDLE,ST_CLOSE, parseAuthHeader,httpGetUri
from binascii import hexlify
import struct,hashlib,base64,traceback,urlparse,random

_proto_map_={}

def registerProtocol(name,pclass):
	global _proto_map_
	cls_name=pclass.__name__+"__WebSocketTransport"
	_proto_map_[name]=type(cls_name,(ProxyChannel,pclass),{'__name__':cls_name})

def getAuthStr(args):
	out=[]
	for key in args:
		out.append('%s=%s' % (key,args[key]))
	return ','.join(out)

class ProxyChannel(object):
	def __init__(self,base_channel,dispatcher,ident,**kwargs):
		self._base_chan=base_channel
		self._dispatcher=dispatcher
		self.id_cache=ident
		self.__pinit__(**kwargs)
	def send(self,packet):
		if not self._base_chan:
			raise AttributeError('ProxyChannel closed, but still alive, do you have a hanging reference??')
		try:
			self.onSend(packet)
			self.doSend()
		except:
			traceback.print_exc()
	def doSend(self):
		if self.producer:
			for packet in self.producer:
				print repr(packet)
				self._base_chan.send(packet)
		else:
			self._base_chan.send(self.out_buffer)
			self.out_buffer=""
	def feed(self,packet):
		#print "feed packet",packet,self.in_buffer
		if self.in_buffer.feed(packet):
			#print "packets waiting"
			self.onData()
	def put(self,event,sync=False):
		if sync:
			try:
				dis_res=self._dispatcher(event,cb=self.onCallback)
			except StopIteration:
				self.onCallback(event)
			except Exception,exc:
				self.onCallback(event,exc=exc)
			else:
				#print 'no exception...',dis_res
				if not dis_res:
					self.onCallback(event)
		else:
			try:
				self._dispatcher(event)
			except:
				traceback.print_exc()
	def close(self):
		print ">>> close in ProxyChannel"
		self.onClose()
		#self._base_chan=None
		#self._dispatcher=None
	def __del__(self):
		print ">>> destruction of ProxyChannel"

ST_UPGRADE=7
ST_WEBSOCK=8

class HYBI13Buffer(SimpleBuffer):
	def __init__(self):
		self.part=""
		super(HYBI13Buffer,self).__init__()
	def feed(self,data):
		self.part=self.part+data
		while len(self.part)>1:
			h_len=2
			opcode,p_len=struct.unpack('!BB',self.part[:2])
			#print opcode,p_len&127,len(self.part)
			try:
				if p_len & 127==126:
					h_len=h_len+2
					p_len=p_len+struct.unpack('!H',self.part[2:4])[0]
				if p_len & 127==127:
					h_len=h_len+8
					p_len=p_len+struct.unpack('!L',self.part[2:10])[0]
				if p_len & 128:
					mask=struct.unpack('!BBBB',self.part[h_len:h_len+4])
					h_len=h_len+4
					ump=''
					#p_len=p_len&127
					#print h_len,p_len,mask
					for ch_ind in range(h_len,h_len+p_len):
						#print ch_ind,ord(self.part[ch_ind]),ch_ind-h_len%4
						try:
							ump=ump+chr(ord(self.part[ch_ind])^mask[(ch_ind-h_len)%4])
						except:
							pass
						#	traceback.print_exc()
					self.append(ump)
				else:
					self.append(self.part[h_len:h_len+p_len])
				self.part=self.part[h_len+p_len:]
			except struct.error:
				break
			except IndexError:
				break
		#print self
		return len(self)

class WebSocketProducer(SimpleProducer):
	def __init__(self,**kwargs):
		kwargs['encoding']=None
		SimpleProducer.__init__(self,**kwargs)
	def append(self,packet):
		#print ">>>>>>",len(packet),packet
		if len(packet)<=125:
			SimpleProducer.append(self,struct.pack('!BB',129,len(packet))+packet)
		elif len(packet)<=65535:
			SimpleProducer.append(self,struct.pack('!BBH',129,126,len(packet))+packet)
		else:
			SimpleProducer.append(self,struct.pack('!BBL',129,127,len(packet))+packet)

class HttpWSClientProtocol(HttpProtocol):
	def __pinit__(self,ws_version=13,**kwargs):
		self.proto_version=ws_version
		#self.con_lck=thread.allocate_lock()
		self.identity=None
		self.credentials=None
		HttpProtocol.__pinit__(self,**kwargs)
	def setIdentity(self,ident,cred):
		self.identity=ident
		self.credentials=cred
	def wsConnect(self,uri,proto):
		if self.pstate!=ST_IDLE:
			raise IOError("channel already connected or old")
		self.curr_uri=urlparse.urlparse(uri)
		if self.curr_uri.port:
			self.connect((self.curr_uri.hostname,self.curr_uri.port))
		elif self.curr_uri.scheme in ('wss','https'):
			self.connect((self.curr_uri.hostname,443))
		else:
			self.connect((self.curr_uri.hostname,80))
		self.send("GET %s HTTP/1.1\r\nHost: %s\r\nUpgrade: websocket\r\nConnection: Upgrade\r\nSec-WebSocket-Protocol: %s\r\nSec-WebSocket-Version: %s\r\n" % (self.curr_uri.path,self.curr_uri.netloc,proto,self.proto_version))
		self.send("Sec-WebSocket-Key: %s\r\n\r\n" % base64.b64encode(struct.pack('d',random.random())))
	def onData(self):
		if self.pstate==ST_WEBSOCK:
			for packet in self.in_buffer:
				try:
					self.sub_proto.feed(packet)
				except ValueError:
					self.close()
		else:
			HttpProtocol.onData(self)
	def onPacket(self,resp,head={},body=''):
		if resp[1]=='101':
			# yay an upgrade response set us into ST_WEBSOCK and setup sub protocol instance...
			self.pstate=ST_WEBSOCK
			self.in_buffer=HYBI13Buffer()
			if _proto_map_.has_key(head['Sec-WebSocket-Protocol']):
				self.sub_proto=_proto_map_[head['Sec-WebSocket-Protocol']](self,self._dispatcher)
				self.sub_proto.onConnect(self.peer)
			else:
				# should be handling error here and informing client...
				raise KeyError("no supported subproto in map")
		elif resp[1]=='401':
			# identity challenge....
			if self.identity and self.credentials:
				auth_type=head['WWW-Authenticate'].split(' ')[0]
				if auth_type=='Basic':
					auth_str='Basic %s' % base64.b64encode('%s:%s' % (self.identity,self.credentials))
				elif auth_type=='Digest':
					auth=parseAuthHeader(head['WWW-Authenticate'].split(' ')[1])
					HA1=hashlib.md5('%s:%s:%s' % (self.identity,auth['realm'],self.credentials))
					# we can fix on GET here as we always use it
					HA2=hashlib.md5('GET:%s' % auth['uri'])
					cnonce=hexlify(struct.pack('I',random.randint()))
					HA=hashlib.md5(':'.join((HA1,auth['nonce'],'00001',cnonce,auth['qop'],HA2)))
					auth.update({'username':self.identity,'cnonce':cnonce,'nc':'00001','uri':'/test.html','response':HA})
					auth_str="Digest %s" % getAuthStr(auth)
				self.send("GET %s HTTP/1.1\r\nHost: %s\r\nUpgrade: websocket\r\nConnection: Upgrade\r\nSec-WebSocket-Protocol: %s\r\nSec-WebSocket-Version: %s\r\n" % (self.curr_uri.path,self.curr_uri.netloc,head['Sec-WebSocket-Protocol'],self.proto_version))
				self.send("Authorization: %s\r\n" % auth_str)
				self.send("Sec-WebSocket-Key: %s\r\n\r\n" % base64.b64encode(struct.pack('d',random.random())))
		else:
			# anything else is an error for us.... we don't have anything to do with any other response....
			# deal with the response and set pstate appropriately
			self.pstate=ST_CLOSE
			raise IOError("got response %s from server" % resp)

class HttpWSProtocol(HttpServerProtocol):
	def __pinit__(self,**kwargs):
		super(HttpWSProtocol,self).__pinit__()
		#self.headers={'Sec-WebSocket-Version':'00'}
		self.proto_version=None
		self.path=None
		self.sub_proto=None
	def onData(self):
		#print "onData",self.pstate
		if self.pstate==ST_WEBSOCK:
			self.onWsData()
		elif self.pstate==ST_UPGRADE:
			self.producer=WebSocketProducer()
			self.pstate=ST_WEBSOCK
			self.onWsData()
		else:
			super(HttpWSProtocol,self).onData()
	def onWsData(self):
		#print "feeding subproto"
		for packet in self.in_buffer:
			#print 'websocket data ->',packet
			try:
				self.sub_proto.feed(packet)
			except ValueError:
				print "ValueError on feed...."
				self.close()
		#self.sub_proto.onData()
	def onCallback(self,event,exc=None):
		#print "Callback in websocket",id(event),event['resp_code'],event['resp_headers']
		if exc:
			event['resp_code']=500
			HttpServerProtocol.onCallback(self,event,exc=exc)
			return
		if event['resp_code']==101:
			self.pstate=ST_UPGRADE
			# TODO reject down level websocket versions...
			if event['req_headers'].has_key('sec-websocket-version'):
				self.proto_version=int(event['req_headers']['sec-websocket-version'])
			else:
				self.proto_version=0
			if self.proto_version>=13:
				#print "WebSocket version -->>",self.proto_version
				self.in_buffer=HYBI13Buffer()
			if _proto_map_.has_key(event['resp_headers']['Sec-WebSocket-Protocol']):
				if event.has_key('identity') and event['validated']:
					ident=(event['identity'],event['credentials'])
				else:
					ident=None
				if evt.has_key('view'):
					view=evt['view']
				else:
					view=None
				if 'dispatcher' in event and event['dispatcher']:
					self.sub_proto=_proto_map_[event['resp_headers']['Sec-WebSocket-Protocol']](self,event['dispatcher'],ident,view=view)
				else:
					self.sub_proto=_proto_map_[event['resp_headers']['Sec-WebSocket-Protocol']](self,self._dispatcher,ident,view=view)
				# technically this is wrong as we don't really "spawn" the sub channel at all,
				# however it is analagous enough to work in most cases
				self.sub_proto.onSpawn(self)
			else:
				# should be handling error here and informing client...
				raise KeyError("no supported subproto in map")
			self.producer.append('HTTP/1.1 101 Switching Protocols\r\nServer: Tao Service Framework v0.1\r\nDate: %s\r\n' % httpDate())
			#print event['req_headers']
			for hkey in event['resp_headers'].keys():
				#print '%s: %s' % (hkey,event['resp_headers'][hkey])
				self.producer.append('%s: %s\r\n' % (hkey,event['resp_headers'][hkey]))
			if event.has_key('port'):
				host="http://%s:%s" % (event['hostname'],event['port'])
			else:
				host="http://"+event['hostname']
			self.producer.append("Access-Control-Allow-Origin: %s\r\nAccess-Control-Allow-Credentials: true\r\n" % host)
			self.producer.append('\r\n')
			#print self.producer
			self.doSend()
		else:
			super(HttpWSProtocol,self).onCallback(event,exc=exc)
	def endHeaders(self):
		if self.curr_headers.has_key('upgrade'):
			# we are being upgraded to something else....
			# override to prevent endHeaders from futzing with pstate
			self.onPacket(self.curr_request,head=self.curr_headers)
			self.curr_request=None
			self.curr_headers={}
		else:
			super(HttpWSProtocol,self).endHeaders()
	def onClose(self):
		#print ">>> Websocket.onClose()"
		try:
			self.sub_proto.close()
		except AttributeError:
			pass
		except:
			print "Exception Closing proxy channel in WebSocket ->\n",traceback.format_exc()
		self.sub_proto=None
		HttpServerProtocol.onClose(self)

class HttpWSClientDispatcher(HttpWSClientProtocol,TrackDispatcher):
	def __pinit__(self,**kwargs):
		HttpWSClientProtocol.__pinit__(self,**kwargs)
	def dispatch(self,evt,cb=None):
		if self.state==ST_WEBSOCK:
			TrackDispatcher.dispatch(self,evt,cb=cb)
			if cb:
				return dispatchWithCallback(self.sub_proto,evt,self.finish)
			else:
				self.sub_proto(evt)
		else:
			#reject this we aren't connected yet
			raise DispatchException(self)
	#def finish


