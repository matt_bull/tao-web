function XhrChannel(target,dataHandler,errorHandler)	{
	this.xmlhttp=new XMLHttpRequest();
	this.xmlhttp.onreadystatechange=this.readyHandler.bind(this);
	this.dataHandler=dataHandler;
	if(errorHandler)	{
		this.errorHandler=errorHandler;
	} else	{
		this.errorHandler=function(msg)	{
			window.console.debug(msg);
		};
	}
	this.target=target;
}
XhrChannel.prototype.request=function(data)	{
	if(this.xmlhttp.readyState==0)	{
		if(data)	{
			this.xmlhttp.open('POST',this.target,true);
		} else	{
			this.xmlhttp.open('GET',this.target,true);
		}
		this.xmlhttp.send();
	} else	{
		// what to do here??
	}
}
XhrChannel.prototype.readyHandler=function()	{
	window.console.debug('ready state - '+this.xmlhttp.readyState+', status - '+this.xmlhttp.status);
	if(this.xmlhttp.readyState==4 && this.xmlhttp.status==200)	{
		switch(this.xmlhttp.status)	{
			case 200:
				headers=this.xmlhttp.getAllResponseHeaders();
				this.dataHandler(headers,this.xmlhttp.response);
				break;
			default:
				this.errorHandler('Unknown resp code '+this.xmlhttp.status+' no idea what to do with that, bailing');
		}
	}
	
}
