// bar javascript
function BarDisplay(w_id,range,warn_dir,warn,danger)	{
	this.widget=document.getElementById(w_id).parentElement;
	this.bar=document.querySelector("#"+w_id+" > .bar");
	this.label=document.querySelector("#"+w_id+" .label");
	this.range=range;
	this.warn_dir=warn_dir;
	if(warn_dir==0)	{
		this.bar.style.backgroundColor='rgb(120 148 230)';
	} else	{
		this.warn=warn;
		this.danger=danger;
	}
	this._setV=function(value)	{
		if(value<0)	{
			value=0;
		}
		if(value>this.range)	{
			value=this.range;
		}
		pc_value=(value/this.range)*100;
		//window.console.debug(pc_value);
		this.bar.style.width=pc_value+'%';
		this.label.innerHTML=value+' / '+this.range;
		if(this.warn_dir>0)	{
			//window.console.debug('high warn');
			if(pc_value>this.danger)	{
				this.bar.style.backgroundColor='#b52424';
				return;
			}
			if(pc_value>this.warn)	{
				this.bar.style.backgroundColor='#e89613';
				return;
			}
			this.bar.style.backgroundColor='#61c263';
		}
		if(this.warn_dir<0)	{
			//window.console.debug('low warn');
			if(pc_value<this.danger)	{
				this.bar.style.backgroundColor='#b52424';
				return;
			}
			if(pc_value<this.warn)	{
				this.bar.style.backgroundColor='#e89613';
				return;
			}
			this.bar.style.backgroundColor='#61c263';
		}
	}
	this.setValue=this._setV.bind(this);
	this.setValue(0);
	this._setR=function(range)	{
		this.range=range;
	}
	this.setRange=this._setR.bind(this);
}

function initBar(id,range,warn_dir,warn,danger)	{
	bar=new BarDisplay(id,range,warn_dir,warn,danger);
	return bar;
}
