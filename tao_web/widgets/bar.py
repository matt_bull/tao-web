from tao_web.web_widget import Widget

WIDGET_HTML='''
<div class='bar_frame' style='width:400px;' id='%s'>
	<div class='bar'>
	<span class='label'>0 / 0</span>
	</div>
</div>
'''

class BarWidget(Widget):
	_embed_paths=['bar.css','bar.js']
	_js_init="initBar('%(id)s',%(range)s,%(warn_dir)s,%(warn)s,%(danger)s)"
	def __init__(self,title=None,w_id=None,warn_dir=-1,warn=25,danger=10,rng=100):
		self.warn_dir=warn_dir
		self.warn=warn
		self.danger=danger
		self.rng=rng
		Widget.__init__(self,title=title,w_id=w_id)
	def getInitArgs(self,doc_uri=None):
		return {'range':self.rng,'warn_dir':self.warn_dir,'warn':self.warn,'danger':self.danger}
	def render(self,data,doc_uri):
		if self.title:
			return ('<div class="bar_title">%s</div>\n' % self.title)+(WIDGET_HTML % self.wid)
		else:
			return WIDGET_HTML % self.wid
	
if __name__=='__main__':
	from tao_web.tests.utils import runTest
	from tao_web.widgets.box import VBox
	box=VBox()
	box.add(BarWidget(w_id='test_bar_1',rng=100))
	box.add(BarWidget(title='Example Reading',w_id='test_bar_2',rng=100))
	box.add(BarWidget(w_id='test_bar_3',warn_dir=1,rng=250))
	box.add(BarWidget(title='Example Reading',w_id='test_bar_4',warn_dir=-1,rng=250))
	runTest(box)

		

