// data binding javascript
var BOUND_MAP={};

function walkTheDOM(nd,func)	{
	func(nd);
	nd = nd.firstChild;
	while(nd)	{
		walkTheDOM(nd,func);
		nd=nd.nextSibling;
	}
}

function SetupBinding()	{
	walkTheDOM(document.body,function (node)	{
		if(node.nodeType===1)	{
			bind=node.getAttribute('bind');
			if(bind)	{
				BOUND_MAP[bind]=node;
			}
		}
	});
}

