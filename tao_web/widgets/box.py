# box widgets
from tao_web.web_widget import ContainerWidget,getCssConstraint

class Box(ContainerWidget):
	_embed_paths=['box.css']
	def __init__(self,child_widgets=[],layout_tags=[],title=None,**kwargs):
		ContainerWidget.__init__(self,child_widgets=child_widgets,title=title,**kwargs)
		if len(layout_tags)>=len(self.children):
			self.child_layout=layout_tags
		else:
			self.child_layout=[]
			while len(self.child_layout)<len(self.children):
				self.child_layout.append('flex')
	def add(self,widget,index=None,size='flex'):
		if index==None:
			self.children.append(widget)
			self.child_layout.append(size)
		else:
			self.children.insert(index,widget)
			self.child_layout.insert(index,size)

class VBox(Box):
	def render(self,data,doc_uri='',layout=None):
		b_style=''
		b_style=b_style+getCssConstraint('width',self.width_constraint)
		b_style=b_style+getCssConstraint('height',self.height_constraint)
		if layout:
			yield "<div class='vbox' style='%s%s'>\n" % (b_style,layout)
		else:
			yield "<div class='vbox' style='width:100%%;%s'>\n" % b_style
		for ind in range(len(self.children)):
			if isinstance(self.children[ind],Box):
				if self.child_layout[ind]=='flex':
					yield self.children[ind].render(data,doc_uri=doc_uri,layout="flex-grow:1;")
				else:
					yield self.children[ind].render(data,doc_uri=doc_uri,layout="height:%s;" % self.child_layout[ind])
			elif self.child_layout[ind]=='flex':
				yield "<div class='box' style='flex-grow:1;%s'>\n" % getCssConstraint('height',self.children[ind].height_constraint)
				yield self.children[ind].render(data,doc_uri=doc_uri)
				yield "</div>\n"
			else:
				yield "<div class='box' style='height:%s;%s'>\n" % (self.child_layout[ind],getCssConstraint('height',self.children[ind].height_constraint))
				yield self.children[ind].render(data,doc_uri=doc_uri)
				yield "</div>\n"
		yield "</div>\n"

class HBox(Box):
	_embed_paths=[]
	def render(self,data,doc_uri='',layout=None):
		b_style=''
		b_style=b_style+getCssConstraint('width',self.width_constraint)
		b_style=b_style+getCssConstraint('height',self.height_constraint)
		if layout:
			yield "<div class='hbox' style='%s%s'>\n" % (b_style,layout)
		else:
			yield "<div class='hbox' style='width:100%%;%s'>\n" % b_style
		for ind in range(len(self.children)):
			if isinstance(self.children[ind],Box):
				if self.child_layout[ind]=='flex':
					yield self.children[ind].render(data,doc_uri=doc_uri,layout="flex-grow:1;")
				else:
					yield self.children[ind].render(data,doc_uri=doc_uri,layout="width:%s;" % self.child_layout[ind])
			elif self.child_layout[ind]=='flex':
				yield "<div class='box' style='flex-grow:1;%s'>\n" % getCssConstraint('width',self.children[ind].width_constraint)
				yield self.children[ind].render(data,doc_uri=doc_uri)
				yield "</div>\n"
			else:
				yield "<div class='box' style='width:%s;%s'>\n" % (self.child_layout[ind],getCssConstraint('width',self.children[ind].width_constraint))
				yield self.children[ind].render(data,doc_uri=doc_uri)
				yield "</div>\n"
		yield "</div>\n"

if __name__=="__main__":
	from tao_web.tests.utils import runTest
	from tao_core import sigIO,sigIO_net
	from tao_web.web_widget import Widget
	#from tao_web.http import HttpServerProtocol
	#import time
	class btest(Widget):
		def __init__(self,title=None,min_h=None,max_h=None):
			Widget.__init__(self,title=title)
			self.height_constraint=(min_h,max_h)
		def render(self,res,doc_uri=''):
			return "<div style='display:block;width:100%%;border:solid blue 2px;text-align:center;vertical-align:bottom;'>Widget - %s</div>\n" % self.title
	class ctest(Widget):
		def __init__(self,title=None,min_w=None,max_w=None):
			Widget.__init__(self,title=title)
			self.width_constraint=(min_w,max_w)
		def render(self,res,doc_uri=''):
			return "<div style='display:block;height:200px;border:solid red 2px;text-align:center;'>Widget - %s</div>\n" % self.title
	io_main=sigIO.getIO()
	io_main.start()
	def monitorView():
		hb=HBox()
		vb=VBox()
		hb.add(ctest(title='HBox Col 1'),size='flex')
		hb.add(ctest(title='HBox Col 2'),size='80%')
		hb.add(ctest(title='HBox Col 3'),size='flex')
		hb.add(ctest(title='HBox Col 4'),size='flex')
		vb.add(btest(title='Box 2',min_h=120))
		vb.add(btest(title='Box 3',min_h=120))
		vb.add(btest(title='Box 4',min_h=120,max_h=250))
		hb.add(vb)
		return hb
	runTest(monitorView())
