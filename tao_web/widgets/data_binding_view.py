from websocket_view import WebSocketView
from tao_core.JEP import JEPProtocol
from tao_core.utils import ListDict
from tao_web import websocket
from tao_core.data_model import DataOp,DataQuery,DATA_GET,DATA_UPDATE,DATA_DELETE,DATA_NEW
#view which takes care of the data transmission for widgets bound to a specific key with bind(key,widget)

# first register jep with websocket
websocket.registerProtocol('JEP',JEPProtocol)

class DataView(WebSocketView):
	_js_init="setupData('%(doc_uri)s','JEP');"
	_proto_name_='JEP'
	def __init__(self,*args,**kwargs):
		WebSocketView.__init__(self,*args,**kwargs)
		self.key_map=ListDict()
	def getWebsocketDispatcher(self):
		return self.jepDispatcher
	def jepDispatcher(self,evt,cb=None):
		if evt.name in self.accept_events or not self.accept_events:
			# restrict events to those in accept events
			dispatchWithCallback(evt,self._dispatcher,cb)
	def bind(self,key,widget):
		if hasattr(widget,'supports_registry'):
			self.key_map.add(key,widget)
	def render(self,data,doc_uri=None):
		# override to output html around body.
		#print body
		page=View.render(self,data,doc_uri)
		if type(page)==str:
			return "<span id='websocket_uri' style='display:none;'>%s</span><span id='ws_conn_state'>(Not Connected)</span>%s" % (self.doc_uri,page)
		else:
			page.insert(0,"<span id='websocket_uri' style='display:none;'>%s</span><span id='ws_conn_state'>(Not Connected)</span>" % (self.doc_uri))
			return page

if __name__=="__main__":
	from tao_core import sigIO,sigIO_net
	from tao_web.http import HttpServerProtocol
	import os,time
	io_main=sigIO.getIO()
	io_main.start()
	def data_handler(evt,cb=None):
		if evt.name=='org.digitao.DataGet':
			evt['record_id']='test'
			for ind in range(1,4):
				evt['data']['test_'+str(ind)]='test_value_'+str(ind)
	#view=XmlBoundView(data_handler,'../tests/xml_test.xml',widget_path=['./'])
	view=DataView(data_handler,'../tests/xml_test.xml',widget_path=['./'])
	chan=io_main.newChannel(HttpServerProtocol,sigIO_net.TCPInetTransport,dispatcher=view)
	chan.listen((os.getenv('IP','0.0.0.0'),int(os.getenv('PORT',8080))))
	while 1:
		try:
			time.sleep(20)
		except KeyboardInterrupt:
			break
	chan.close()
	io_main.stop()

