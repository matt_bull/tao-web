function Dialog(_id,ent_ids,modal)	{
	this.main=document.getElementById(_id);
	this._id=_id
	if(modal)	{
		this.modal=document.createElement('div');
		document.body.appendChild(this.modal);
		this.modal.hidden=true;
		this.modal.className='twwmodal';
	}
	this.entries={}
	for(var i=0;i<ent_ids.length;i++)	{
		ent=eval('__tww_'+ent_ids[i]+'__');
		window.console.debug(ent,ent_ids[i])
		if(ent.label==undefined && ent.hasAttribute('label'))	{
			this.entries[ent.getAttribute('label')]=ent;
		} else {
			this.entries[ent.label.innerText]=ent;
		}
	}
	this.data='';	
}
Dialog.prototype.show=function(data)	{
	if(this.modal!=undefined)	{
		this.modal.hidden=false;
	}
	this.main.style.display='block';
	this.main.style.position='fixed';
	this.main.style.left=Math.round((window.innerWidth-this.main.clientWidth)/2)+'px';
	this.main.style.top=Math.round((window.innerHeight-this.main.clientHeight)/2)+'px';
	this.data=data;
}
Dialog.prototype.hide=function()	{
	if(this.modal!=undefined)	{
		this.modal.hidden=true;
	}
	this.main.style.display='none';
}
Dialog.prototype.setTitle=function(title)	{
	this.main.firstElementChild.innerText=title;
}

function getDialog(d_id)	{
	for(var i=0;i<_dialogs.length;i++)	{
		if(_dialogs[i]._id==d_id)	{
			return _dialogs[i];
		}
	}
}

function closeDialog(d_id)	{
	getDialog(d_id).hide();
}

var _dialogs=[];

function setupDialog(modal,wid,entry_ids)	{
	_dialogs.push(new Dialog(wid,entry_ids,modal));
	// we now have to return the newly created dialog
	return _dialogs[_dialogs.length-1];
}

