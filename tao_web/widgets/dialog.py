from tao_web.web_widget import ContainerWidget,Widget
from tao_web.widgets.entry import EntryWidget
import sys

class Dialog(ContainerWidget):
	_embed_paths=['dialog.js','dialog.css','tool.css']
	def __init__(self,child_widgets=[],title=None,actions=[],show_close='Close',modal=False,**kwargs):
		ContainerWidget.__init__(self,child_widgets=child_widgets,title=title,**kwargs)
		self.actions=actions	# actions should be dict of titles to javascript functions
		self.show_close=show_close
		self.modal=modal
	def getEntries(self,elem=None):
		#print 'getEntries for',elem
		if not elem:
			elem=self
		try:
			ents=[]
			for child in elem.children:
				#sys.modules[cls.__module__].__file__
				#print child.__class__.__module__,EntryWidget.__module__
				#print sys.modules[child.__class__.__module__].__file__,sys.modules[EntryWidget.__module__].__file__,isinstance(child,EntryWidget)
				if isinstance(child,EntryWidget):
					ents.append(child.wid)
				else:
					ents.extend(self.getEntries(elem=child))
			return ents
		except AttributeError:
			return []
	def getJsInit(self,doc_uri):
		# we can get away with this fairly easily as 
		entries=self.getEntries()
		js_init_strings=[]
		for child in self.children:
			js_init_strings.extend(child.getJsInit(doc_uri))
		if self.modal:
			js_init_strings.append("__tww_%(id)s__=setupDialog(true,'%(id)s',%(ent)s);" % {'id':self.wid,'ent':repr(entries)})
		else:
			js_init_strings.append("__tww_%(id)s__=setupDialog(false,'%(id)s',%(ent)s);" % {'id':self.wid,'ent':repr(entries)})
		return js_init_strings
	def renderBody(self,data,doc_uri=None):
		return "test"
	def render(self,data,doc_uri=''):
		#print "rendering dialog",self.actions,self.show_close
		yield "<div id='%s' class='dialog'>\n\t<div class='header' style='padding:5px;'>%s</div><div style='padding:5px;padding-bottom:35px;align-items:center;justify-content:center;'>" % (self.wid,self.title)
		if self.children:
			for cw in self.children:
				yield cw.render(data,doc_uri=doc_uri)
		else:
			yield self.renderBody(data,doc_uri=doc_uri)
		yield "</div>\n"
		if self.actions:
			html="<div class='toolbar' style='position:absolute;bottom:0;width:100%;text-align:right;'>"
			for action_title in self.actions:
				html=html+"<div class='button' onclick='%s()'>%s</div>" % action_title
			yield html
		else:
			yield "<div class='toolbar' style='position:absolute;bottom:0;width:100%;text-align:right;'>"
		if self.show_close:
			yield "<div class='button' onclick='closeDialog(%s)'>%s</div>" % ('"%s"' % self.wid,self.show_close)
		yield "</div>\n</div>\n"

if __name__=="__main__":
	from tao_web.tests.utils import runTest
	from tao_core import sigIO,sigIO_net
	#from tao_web.web_widget import View
	#from tao_web.http import HttpServerProtocol
	from tao_web.widgets import tools
	import time,os
	io_main=sigIO.getIO()
	io_main.start()
	dia_1=Dialog(title='Test Dialog',show_close='Cancel',actions=[('test()','Test')])
	cont=ContainerWidget()
	cont.add(dia_1)
	cont.add(tools.Button(title='Open Dialog',action='_dialogs[0].show()'))
	runTest(cont)
	#chan=io_main.newChannel(HttpServerProtocol,sigIO_net.TCPInetTransport,dispatcher=view)
	#chan.listen((os.getenv('IP','0.0.0.0'),int(os.getenv('PORT',8080))))
	#while 1:
	#	try:
	#		time.sleep(20)
	#	except KeyboardInterrupt:
	#		break
	#chan.close()
	#io_main.stop()
