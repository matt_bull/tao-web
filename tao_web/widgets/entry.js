var data_map={};

function Entry(_id,pr,bk)	{
	this.activated=false;
	this.bound_key=bk;
	this.protect=pr
	this.main=document.getElementById(_id);
	//this.main.onkeyup=this.edit.bind(this);
	this.main.onkeydown=this.edit.bind(this);
	this.main.onkeyup=this.textprotect.bind(this);
	//this.main.onfocusout=this.focusout.bind(this);
	for(var i=0;i<this.main.children.length;i++)	{
		switch(this.main.children[i].className)	{
			case 'label':
				this.label=this.main.children[i];
				break;
			case 'textbox':
				this.entry=this.main.children[i];
				break;
		}
	}
	if(this.entry.innerText.length>0)	{
		this.activated=true;
		this.label.style.fontSize='8pt';
	}
	this.entry.onblur=this.focusout.bind(this);
	this.value=this.entry.innerText;
	if(this.bound_key)	{
		if(!document.form_data)	{
			document.form_data={}
		}
		document.form_data[this.bound_key]=this.value;
	}
	if(bk!=undefined)	{
		data_map[bk]=this;
	}
}

var IgnoreKeys=["ArrowUp","ArrowLeft","ArrowRight","ArrowDown","Home","End","Shift","Control","Alt","Tab"]

Entry.prototype.edit=function(evt)	{
	if(!this.activated)	{
		this.activated=true;
		this.label.style.fontSize='8pt';
	}
	//console.info('>>>'+this.entry.innerText);
	if(IgnoreKeys.includes(evt.key))	{
		return;
	}
	if(this.protect)	{
		slct=window.getSelection();
		window.console.debug(slct);
		window.console.debug('>>'+evt.key);
		if(evt.key=="Enter")	{
			// what to do here
			return
		}
		offset=Math.min(slct.anchorOffset,slct.focusOffset);
		if(evt.key=="Backspace")	{
			p1=this.value.substring(0,offset-1);
			p2=this.value.substring(offset+slct.toString().length);
			window.console.debug(p1+' - '+p2)
			this.value=p1+p2;
		} else	{
			window.console.debug(slct.anchorOffset);
			p1=this.value.substring(0,offset);
			p2=this.value.substring(offset+slct.toString().length);
			this.value=p1+evt.key+p2;
		}
		window.console.debug(this.value)
	}else	{
		this.value=this.entry.innerText+evt.key;
	}
}

Entry.prototype.textprotect=function(evt)	{
	if(IgnoreKeys.includes(evt.key))	{
		return;
	}
	if(this.protect)	{
		slct=window.getSelection();
		if(evt.key!="Backspace")	{
			p1=this.value.substring(0,slct.anchorOffset-1);
			p2=this.value.substring(slct.anchorOffset);
			window.console.debug(slct.anchorOffset)
			if(p1.length>0)	{
				slct.anchorNode.replaceData(0,p1.length,'\u2022'.repeat(p1.length));
				slct.anchorNode.replaceData(p1.length+1,p2.length,'\u2022'.repeat(p2.length));
				window.console.debug(slct.anchorOffset)
			}
		}
		window.console.debug(this.value)
	}
}

Entry.prototype.focusout=function()	{
	if(this.protect)	{
		this.entry.innerText='\u2022'.repeat(this.value.length);
	}
}

function SelectEntry(_id,bk,val)	{
	this.bound_key=bk
	this.main=document.getElementById(_id);
	this.main.onclick=this.toggleMenu.bind(this);
	for(elem of this.main.children)	{
		if(elem.className=='label')	{
			this.label=elem;
		}			
		if(elem.className=='menu')	{
			this.menu=elem;
			for(mi of elem.children)	{
				mi.onclick=this.select.bind(this,mi);
			}
		}
	}
	//window.console.debug('select value',val);
	if(val!=undefined)	{
		if(val.length>0)	{
			this.activated=true;
			this.label.style.fontSize='8pt';
		}
	}
	this.value=val;
	if(bk!=undefined)	{
		data_map[bk]=this;
	}
}
SelectEntry.prototype.toggleMenu=function ()	{
	var tb;
	for(elem of this.main.children)	{
		if(elem.className=='label')	{
			if(!elem.activated)	{
				elem.activated=true;
				elem.style.fontSize='8pt';
			}
		}
		if(elem.className=='selectbox')	{
			tb=elem;
		}
		if(elem.className=='menu')	{
			//window.console.debug(menu.style.opacity);
			if(elem.style.opacity=='1')	{
				elem.style.opacity='0';
			} else	{
				elem.style.opacity='1';
			}
		}
	}
}
SelectEntry.prototype.setEntries=function(ents)	{
	while (this.menu.firstChild) {
		this.menu.removeChild(this.menu.firstChild);
	}
	/*for(item of this.menu.children)	{
		this.menu.removeChild(item);
	}*/
	//window.console.debug(this.menu.children);
	for(item of ents)	{
		var elem=document.createElement('li');
		elem.key=item[0];
		elem.innerText=item[1];
		elem.onclick=this.select.bind(this,elem);
		this.menu.appendChild(elem);
	}
}
SelectEntry.prototype.select=function(elem)	{
	window.console.debug(elem.textContent);
	elem.parentElement.previousElementSibling.innerHTML=elem.innerHTML;
	this.value=elem.key;
}


