from tao_web.web_widget import Widget,XmlTag

TEXT_TMPL="""<div style='width:%s;position:relative;padding:0px;height:27pt;margin:2pt;' id='%s'>
	<div class='label'>%s</div>
	<div class='textbox' contenteditable='true' style='position:relative;'>%s</div>
</div>"""

class EntryWidget(Widget):
	_embed_paths=['entry.js','entry.css']
	_js_init="new Entry('%(id)s',%(pr)s,'%(bk)s');"
	def __init__(self,title='',width='70%',hide=False):
		Widget.__init__(self,title=title)
		self.width=width
		if hide:
			self.hide='true'
		else:
			self.hide='false'
	def getInitArgs(self,doc_uri=None):
		if self.bound_key:
			return {'pr':self.hide,'bk':self.bound_key}
		else:
			return {'pr':self.hide,'bk':'undefined'}
	def render(self,data,doc_uri=''):
		#print "Entry.render",data,self.bound_key
		if self.bound_key and data.has_key(self.bound_key):
			value=data[self.bound_key]
		elif data and type(data)==str:
			value=data
		else:
			value=''
		return TEXT_TMPL % (self.width,self.wid,self.title,value)

class HintEntryWidget(EntryWidget):
	_embed_paths=['hints.js']
	def __init__(self,title='',width='70%',hints=[]):
		self.hints=hints
		EntryWidget.__init__(self,title=title,width=width)
	def render(self,data,doc_uri=''):
		yield EntryWidget.render(self,data,doc_uri=doc_uri)+"\n\t<ul class=menu>"
		for hint in self.hints:
			yield "\n\t\t<li>%s</li>" % hint
		yield '\n\t</ul>'

class SelectWidget(EntryWidget):
	_embed_paths=['menu.css']
	init_tmpl="""
<div style='width:%s;position:relative;padding:0px;height:27pt;margin:2pt;' id='%s'>
	<div class='label'>%s</div>
	<div class='selectbox' style='cursor:default;' style='position:relative;'>%s</div>
		<ul class='menu'>"""
	_js_init="new SelectEntry('%(id)s','%(bk)s','%(value)s');"
	def __init__(self,title='',width='70%',options={}):
		EntryWidget.__init__(self,title=title)
		self.width=width
		self.options=options
		self.option=None
	def add(self,obj):
		# if this is an instance of an xml tag... we are being loaded from an xml template...
		if isinstance(obj,XmlTag) and obj.tag=='option':
			self.options[obj.attr['value']]=obj.getText()
		else:
			self.options[obj[0]]=obj[1]
	def getInitArgs(self,doc_uri=None):
		args=EntryWidget.getInitArgs(self,doc_uri=doc_uri)
		if self.option:
			args['value']=self.option
		else:
			args['value']=''
		return args
	def render(self,data,doc_uri=''):
		#print "SelectEntry.render",data,self.bound_key
		if self.bound_key and data.has_key(self.bound_key):
			self.option=data[self.bound_key]
		elif data and type(data)==str:
			self.option=data
		else:
			self.option=''
		if self.option in self.options.keys():
			value=self.options[self.option]
		else:
			value="<span style='font-size:14pt;color:#ccc;'>Select</span>"
		wi_str=self.init_tmpl % (self.width,self.wid,self.title,value)
		for option in sorted(self.options.items()):
			wi_str=wi_str+"<li key='%s'>%s</li>" % option
		return wi_str+"\n\t</ul>\n</div>"

if __name__=='__main__':
	from tao_web.tests.utils import runTest
	from widgets import box
	vbox=box.VBox()
	vbox.width_constraint=(100,300)
	e1=EntryWidget(title='Username / Email')
	e1.bind('user')
	vbox.add(e1)
	e2=EntryWidget(title='Password',hide=True)
	e2.bind('password')
	vbox.add(e2)
	s1=SelectWidget(title='Option Entry',options={'opt1':'Option 1','opt2':'Option 2','opt3':'Option 3','opt4':'Option 4','bar':'FooBar'})
	s1.bind('foo')
	vbox.add(s1)
	runTest(vbox)

