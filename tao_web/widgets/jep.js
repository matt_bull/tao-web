//JEP javascript library

var OP_REG=0x01;
var OP_UNREG=0x02;
var OP_EVT=0x03;
var OP_CB=0x04;
var OP_CONT=0x05;
var OP_REL=0x06;

var FL_EXC=0x40;
var FL_CB=0x40;
var FL_FINAL=0x80;

function jepConnHandler(state,msg)	{
	window.console.debug('Connection State Change - '+msg);
}

function jepDataHandler(event)	{
	switch(event[0])	{
		window.console.debug('jep event ->'+JSON.stringify(event));
		case OP_EVT:
			dispatch_map[event[1]](event[2]);
			break;
		default:
			window.console.warn('Unhandled Event - '+JSON.stringify(event));
	}
}

function serverEventDispatcher(event)	{
	switch(event.name)	{
		case "ServerRegister":
			ind=dispatch_map.push(event.handler)-1;
			jepSocket.send([OP_REG,event.ev_name,ind]);
			break;
		case "ServerUnreg":
			disp_ind=dispatch_map.indexOf(event.handler);
			if(disp_ind>=0)	{
				delete dispatch_map[disp_ind];
				jepSocket.send([OP_UNREG,event.ev_name,ind]);
			} else	{
				jepSocket.send([OP_UNREG,event.ev_name]);
			}
			break;
		case "ServerEvent":
			jepSocket.send([OP_EVT,0,event.ev_name,event.ev_data]);
			break;
	}
}

class ServerEvent extends Event	{
	constructor(ev_name,ev_data)	{
		super("ServerEvent",{'bubbles':true})
		this.ev_name=ev_name;
		this.ev_data=ev_data;
	}
}

class ServerRegEvent extends Event	{
	constructor(ev_name,handler)	{
		super("ServerReg",{'bubbles':true})
		this.ev_name=ev_name;
		this.handler=handler;
	}
}

class ServerUnregEvent extends Event	{
	constructor(ev_name,handler)	{
		super("ServerUnreg",{'bubbles':true})
		this.ev_name=ev_name;
		this.handler=handler;
	}
}

function registerDispatcher(evt_name,handler)	{
	dispatch_map[evt_name]=handler;
}

function setupJEP(uri,default_handler)	{
	document.addEventListener("ServerEvent",serverEventDispatcher);
	document.addEventListener("ServerRegister",serverEventDispatcher);
	document.addEventListener("ServerUnreg",serverEventDispatcher);
	var dispatch_map={'__default__':default_handler};
	var jepSocket=setupData(uri,'JEP',NULL,jepDataHandler,jepConnHandler);
}
