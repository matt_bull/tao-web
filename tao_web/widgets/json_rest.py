from tao_web.web_widget import Widget
from tao_core.tdf_core import Dispatcher
import json,os,stat,mimetypes
try:
	import filetype
except:
	filetype=None

class JsonRestCollection(Dispatcher):
	accept_events=['org.digitao.tao_web.HttpRequest']
	def __init__(self,collection_uri):
		self.collection_uri=collection_uri
	def dispatch(self,evt,cb=None):
		if evt['req_type']=='GET':
			if len(evt['disp_path'])==0:
				evt['resp_type']='application/json'
				evt['resp_body']=json.dumps(self.listCollection())
			elif len(evt['disp_path'])==1:
				body,mime=self.getMember(evt['disp_path'][0])
				if not mime and type(body) not in (str,unicode,file):
					evt['resp_body']=json.dumps(body)
					evt['resp_type']='application/json'
				else:
					evt['resp_body']=body
					evt['resp_type']=mime
			else:
				raise ValueError('too many elements in disp_path')
		elif evt['req_type']=='POST':
			if len(evt['disp_path'])==0:
				# new member
				if evt['req_query']:
					data=evt['req_query']
				else:
					data=evt['req_body']
				new_id=self.newMember(data)
				evt['resp_body']={'member_id':new_id}
				evt['resp_headers']['location']=new_id
			else:
				raise ValueError('too many elements in disp_path')
		elif evt['req_type'] in ('PUT','PATCH'):
			# we treat these the same..... not really restful but....
			if len(evt['disp_path'])==1:
				if evt['req_query']:
					data=evt['req_query']
				else:
					data=evt['req_body']
				evt['resp_body']=json.dumps(self.updateMember(evt['disp_path'][0],data))
			elif len(evt['disp_path'])==0:
				# we don't support bulk updating of the collection...
				raise NotImplemented('bulk update of collections not supported')
			else:
				raise ValueError('too many elements in disp_path')
		elif evt['req_type']=='DELETE':
			self.delMember(evt['disp_path'][0])
		else:
			raise ValueError('unknown request type %s' % evt['req_type'])
	def newMember(self,data,content_type,member_id=None):
		# override to create new member in collection from data content-type (should be a mime-type string) and optionally requested member_id, return created member ID
		raise NotImplemented('newMember not implemented in RestCollection %s' % repr(self))
	def getMember(self,member_id):
		# return (body,mime-type) tuple, return (object,None) for data object
		raise NotImplemented('getMember not implemented in RestCollection %s' % repr(self))
	def updateMember(self,member_id,data):
		# return dict of changed keys / values
		raise NotImplemented('updateMember not implemented in RestCollection %s' % repr(self))
	def delMember(self,member_id):
		raise NotImplemented('delMember not implemented in RestCollection %s' % repr(self))
	def listCollection(self):
		# return list of collection member_id's
		raise NotImplemented('listCollection not implemented in RestCollection %s' % repr(self))

def guessFileType(path):
	mime,enc=mimetypes.guess_type(path)
	if filetype and not mime:
		mime=filetype.guess(path)
	return (mime,enc)

class FilesystemCollection(JsonRestCollection):
	def __init__(self,collection_uri,base_dir):
		JsonRestCollection.__init__(self,collection_uri)
		self.base_dir=base_dir
	def newMember(self,data,content_type,member_id=None):
		if content_type=='application/json':
			# this will have been automagically converted into an object... re-encode for a file
			# TODO: we should really use the os default locale for encoding here...
			data=json.dumps(data)
		if not member_id:
			# TODO: generate unique file name (maybe hash of content??)
			raise NotImplemented('unique member_id generation not implemented in FilesystemCollection')
		if '.' in member_id:
			member_id,f_ext=member_id.rsplit('.',1)
		else:
			# should we be generating the file extension dynamically here from the mime-type??
			f_ext=''
		if member_id in os.listdir(self.base_dir):
			# we already have one of those... create a new id derived from this...
			alt_v=1
			while 1:
				if f_ext:
					p_fname="%s(%d).%s" % (member_id,alt_v,f_ext)
				else:
					p_fname="%s(%d)" % (member_id,alt_v)
				if p_fname not in os.listdir(self.base_dir):
					member_id=p_fname
					break
		else:
			if f_ext:
				member_id="%s.%s" % (member_id,f_ext)
		f=open(os.path.join(self.base_dir,member_id),'w')
		f.write(data)
		f.close()
		return member_id
	def getMember(self,member_id):
		mime,enc=guessFileType(os.path.join(self.base_dir,member_id))
		if enc:
			mime=mime+';encoding='+enc
		return (open(os.path.join(self.base_dir,member_id)).read(),mime)
	def delMember(self,member_id):
		os.unlink(os.path.join(self.base_dir,member_id))
	def listCollection(self):
		members=[]
		for fname in os.listdir(self.base_dir):
			if stat.S_ISREG(os.stat(os.path.join(self.base_dir,fname))[0]):
				members.append(fname)
		return ('application/json',json.dumps(members))

	
