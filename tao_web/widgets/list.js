
function List(w_id,row_click)	{
	this.ext_handler=row_click;
	this.tab=document.getElementById(w_id);
	this.selected_ids=[];
	data_rows=this.tab.getElementsByClassName('data_row')
	console.debug('initialise in '+w_id,row_click);
	for(var elem_ind=0;elem_ind<data_rows.length;elem_ind++)	{
		console.debug('>>>'+elem_ind+data_rows[elem_ind]);
		elem=data_rows[elem_ind];
		elem.onclick=this.rowclick.bind(this,elem);
		check=elem.cells[0].childNodes[0];
		if(check!=undefined)	{
			check.onchange=this.selectchanged.bind(this,check)
		}
	}
	var select_inputs=this.tab.getElementsByClassName('select_check')
	for(var elem_ind in select_inputs)	{
		select_inputs[elem_ind].onchange=this.selectchanged.bind(this,select_inputs[elem_ind]);
	}
	this.selected_ids=new Array(0);
}

List.prototype.rowclick=function(row)	{
	//console.debug(row);
	if(this.ext_handler!=undefined)	{
		this.ext_handler(this,row);
	} else	{
		var check=row.cells[0].childNodes[0];
		if(check.checked)	{
			check.checked=false;
		} else	{
			check.checked=true;
		}
		this.selectchanged(check);
	}
}

List.prototype.selectchanged=function(elem)	{
	var row=elem.parentNode;
	while(row.tagName!='TR'){
		row=row.parentNode;
	}
	if(elem.checked)	{
		row.className='selected';
		this.selected_ids.push(row.id);
	} else	{
		this.selected_ids.pop(this.selected_ids.indexOf(row.id));
		row.className='';
	}
	tbs=this.tab.getElementsByClassName('toolbar');
	console.debug(tbs);
	for(var i=0;i<tbs.length;i++)	{
		console.debug(tbs[i],tbs[i].children[0].childElementCount);
		for(var ii=0;ii<tbs[i].children[0].childElementCount;ii++)	{
			console.debug(tbs[i].children[0].children[ii].enableOnSelect);
			if(tbs[i].children[0].children[ii].hasAttribute('enableOnSelect'))	{
				console.debug('activate',tbs[i].children[0].children[ii]);
				if(this.selected_ids.length>0)	{
					tbs[i].children[0].children[ii].disabled=false;
				} else	{
					tbs[i].children[0].children[ii].disabled=true;
				}
			}
		}
	}
}

List.prototype.insertRow=function(index,row_elem)	{
	row_elem.onclick=this.rowclick.bind(this,row_elem);
	data_rows=this.tab.getElementsByClassName('data_row');
	if(index>=data_rows.length)	{
		sib=data_rows[data_rows.length-1];
	} else	{
		sib=data_rows[index];
	}
	sib.insertAdjacentElement('beforeBegin',row_elem);
}

List.prototype.removeRow=function(row_id)	{
	row=this.getRow(row_id);
	if(row!=undefined)	{
		this.tab.tBodies[0].removeChild(row);
	}
}

List.prototype.getRow=function(row_id)	{
	data_rows=this.tab.getElementsByClassName('data_row');
	for(var elem_ind=0;elem_ind<data_rows.length;elem_ind++)	{
		//console.debug('>>>'+elem_ind+data_rows[elem_ind]);
		if(data_rows[elem_ind].id.startsWith(row_id))	{
			return data_rows[elem_ind];
		}
	}
}

List.prototype.moveRow=function(row_id,index)	{
	data_rows=this.tab.getElementsByClassName('data_row');
	target=this.getRow(row_id);
	window.console.debug('move - ',target);
	this.tab.children[0].insertBefore(target,data_rows[0]);
}

var _lists=[];

function setupList(wid,click_handler)	{
	_lists.push(new List(wid,click_handler));
	return _lists[_lists.length-1];
}

