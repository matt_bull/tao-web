from tao_web.web_widget import Widget,SimpleTemplateWidget,View
from tao_web.widgets.dialog import Dialog
from tao_web.widgets.websocket_view import WebSocketView
from tao_core import sigIO
from tao_web import websocket
import json

class List(Widget):
	_embed_paths=['list.js','list.css','tool.css']
	def __init__(self,cols=[],selectable=True,onselect=None,actions=[]):
		Widget.__init__(self)
		self.select=selectable
		self.onselect=onselect
		# cols should be list of tuples with name,width
		self.cols=cols
		self.actions=actions #actions should be tuples of (<function>,<label>,<depends_on_select>)
		self.bound_key=None
	def add_col(self,name='',width=None):
		''' added for compatibility with Hypertext Widget Template spec'''
		self.cols.append((name,width))
	def render(self,data,doc_uri=''):
		if self.bound_key:
			try:
				return self.generate(data[self.bound_key],doc_uri=doc_uri)
			except KeyError:
				print 'Error -> Unable to find key %s in %s' % (self.bound_key,data.keys())
		else:
			return self.generate(data,doc_uri=doc_uri)
	def generate(self,data,doc_uri=''):
		row_ind=0
		yield "<table id='%s' class='list'>\n" % self.wid
		if self.cols:
			if self.select:
				yield "<tr><th width='1%'><center>..</center></th>"
			else:
				yield "<tr>\n"
			for col in self.cols:
				if col[1]:
					yield "<th width='%s'>%s</th>" % (col[1],col[0])
				else:
					yield "<th>%s</th>" % col[0]
			yield "</tr>\n"
		#for row in data:
		#	if row[0]==None:
		#		row=list(row)
		#		row[0]=self.wid+'-'+str(row_ind)
		#	row_ind=row_ind+1
		for row in data:
			#print self,row
			yield self.renderRow(row)
		if self.actions:
			yield "<tr class='toolbar'>\n<td colspan=99 style='text-align:right;'>"
			for action in self.actions:
				if action[2]:
					yield "<button class='button' enableOnSelect onclick='%s(this)' disabled>%s</button>" % action[:2]
				else:
					yield "<button class='button' onclick='%s'>%s</button>" % action[:2]
			yield "</td>\n</tr>\n"
		yield "</table>\n"
	def renderRow(self,row):
		row_t=["<tr id='%s' class='data_row' selected=false>" % row[0]]
		if self.select:
			row_t.append("<td><input name='%s' type='checkbox' onclick='event.stopPropagation()' /></td>" % row[0])
		col=0
		for elem in row[1:]:
			row_t.append("<td id='col%s'>%s</td>" % (col,elem))
			col=col+1
		row_t.append("</tr>\n")
		return ''.join(row_t)
	def getJsInit(self,doc_uri):
		if self.onselect:
			init="setupList('%s',%s)" % (self.wid,self.onselect)
		else:
			init="setupList('%s')" % (self.wid)
		return [init]

class ListView(WebSocketView):
	_proto_name_='json_data_bind'
	_embed_paths=['dynamic_list.js']
	_list_widget=List
	_rowclick=None
	_data_handler='undefined' # this gets passed straight into js_init... this is javascripts version of None
	_conn_handler='undefined'
	def __init__(self,data_source,doc_uri=None,title=None,cols=[],new_template=None,item_template=None,actions=[]):
		WebSocketView.__init__(self,data_source,doc_uri=doc_uri,title=title)
		#actions=[]
		if new_template:
			actions.append(('new_item','New'))
			cont=SimpleTemplateWidget(new_template)
			dia=Dialog(child_widgets=[cont],title='Create New',actions=[('create_new','Add')])
			dia.actions.append((dia.wid+'.hide()','Cancel'))
			self.children.append(dia)
		if item_template:
			cont=SimpleTemplateWidget(new_template)
			dia=Dialog(child_widgets=[cont],title='Edit',actions=[('edit','Add')])
			dia.actions.append((dia.wid+'.hide()','Cancel'))
			self.children.append(dia)
		self.list=self._list_widget(cols=cols,actions=actions,onselect=self._rowclick)
		self.children.append(self.list)
		self.proto_init='ATTACH'
	def dispatch(self,event,cb=None):
		print("ListView ->",event['req_headers'])
		if event.name=="org.digitao.tao_web.HttpRequest":
			return WebSocketView.dispatch(self,event,cb=cb)
		elif event.name=="org.digitao.tao_web.DataUpdate":
			#print "Data Update!!! >>",event.getDict()
			return WebSocketView.dispatch(self,event,cb=cb);
		else:
			pass
	def getJsInit(self,doc_uri):
		js_init_strings=[]
		for child in self.children:
			js_init_strings.extend(child.getJsInit(doc_uri))
		js_init_strings.append("setupListView('%s','%s','%s','%s',%s,%s)" % (doc_uri,self._proto_name_,self.proto_init,self.list.wid,self._data_handler,self._conn_handler))
		return js_init_strings
	def render(self,data,doc_uri=None):
		#print '>>>>> render >>',data
		yield "<span id='websocket_uri' style='display:none;'>%s</span><span id='ws_conn_state'>(Not Connected)</span>" % (self.doc_uri)
		for child in self.children:
			if isinstance(child,Dialog):
				yield child.render([{}],doc_uri=doc_uri)
			else:
				yield child.render(data,doc_uri=doc_uri)

class DataBindProtocol(sigIO.Protocol):
	__in_buffer_class__=sigIO.SimpleBuffer
	def onData(self):
		for packet in self.in_buffer:
			p_data=json.loads(packet)
			self.put(SimpleEvent('org.digitao.tao_web.DataUpdate',**p_data))
	def onCallback(self,evt,exc=None):
		if exc:
			self.send(json.dumps({'error':True,'exception':repr(exc)}))
		else:
			self.send(json.dumps(evt.getDict()))

websocket.registerProtocol('json_data_bind', DataBindProtocol)

if __name__=='__main__':
	from tao_web.tests.utils import runTest
	from tao_core import sigIO,sigIO_net
	from tao_web.web_widget import View
	#from tao_web.http import HttpServerProtocol
	from tao_web.widgets import tools
	import time,random
	io_main=sigIO.getIO()
	io_main.start()
	list_dat=[]
	for ind in range(20):
		list_dat.append(('dp_%s' % ind,"<a href='http://localhost/dp/test/history' target='_blank'>Patient %s</a>" % ind,random.randint(90,100),'%s/%s' % (random.randint(70,100),random.randint(110,150)),'-','Today 9:15am'))
	# test widget
	def dataDispatcher(evt,cb=None):
		evt['result']=list_dat
	class TestListView(ListView):
		_embed_paths=['../tests/test.js']
		_rowclick='click_test'
	view=TestListView(dataDispatcher,doc_uri='http://localhost:8080/list_test',title='test view',
		cols=[('Patient','40%'),('Weight','15%'),('NiBP','15%'),('Glucose','15%'),('Date / Time','15%')],
		actions=[('markReviewed','Reviewed',True),('deleteReadings','Remove',False)],)
	chan=io_main.newChannel(websocket.HttpWSProtocol,sigIO_net.TCPInetTransport,dispatcher=view)
	chan.listen(('0.0.0.0',8080))
	while 1:
		try:
			time.sleep(20)
		except KeyboardInterrupt:
			break
	chan.close()
	io_main.stop()

