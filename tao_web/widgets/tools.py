from tao_web.web_widget import Widget,ContainerWidget

class Button(Widget):
	_embed_paths=['tool.css']
	def __init__(self,title=None,action='',active=True):
		Widget.__init__(self,title=title)
		self.action=action
		self.active=active
	def render(self,data,doc_uri=None):
		if self.action:
			act_str=" onclick='%s'" % self.action
		else:
			act_str=''
		if active:
			return "<button class='button'%s>%s</button>" % (act_str,self.title)
		else:
			return "<button class='button' disabled=true%s>%s</button>" % (act_str,self.title)

class Toolbar(ContainerWidget):
	_embed_paths=['tool.css']
	def render(self,data,doc_uri=None):
		html="<div class='toolbar'>"
		for cw in self.children:
			html=html+cw.render(data,doc_uri)
		return html+"</div>"

if __name__=="__main__":
	from tao_web.tests.utils import runTest
	tb=Toolbar()
	tb.add(Button(title='TB1',action='console.debug("tb1 pressed");'))
	tb.add(Button(title='TB2',action='console.debug("tb2 pressed");'))
	runTest(tb,uri='http://localhost:8080/tools_test')

