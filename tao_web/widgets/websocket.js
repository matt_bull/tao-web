var CS_INP=0;
var CS_CONN=1;
var CS_DIS=2;
var CS_ERR=3;
var timeouts=[10,5000,15000,30000,60000,5*60000]

function TaoWebSocket(uri,protocol,msg_handler,conn_handler,init_packet)	{
	//var conn_handler=conn_handler;
	//var msg_handler=msg_handler;
	// check we got the right protocol
	if(uri.slice(0,4)=='http')	{
		uri='ws'+uri.slice(4);
	}
	this.conn_handler=conn_handler;
	this.msg_handler=msg_handler;
	this.uri=uri
	this.protocol=protocol
	this.p_init=init_packet;
	this.connect();
	this.retrys=0
}
TaoWebSocket.prototype.connect=function()	{
	if(this.ws)	{
		if(this.ws.active)	{
			return;
		}
	}
	this.ws=new WebSocket(this.uri,this.protocol);
	this.conn_handler(CS_INP,this.uri);
	this.ws.active=true;
	this.ws.onopen=this.onOpen.bind(this);
	this.ws.onerror=this.onError.bind(this);
	this.ws.onmessage=this.onMessage.bind(this);
	this.ws.onclose=this.onClose.bind(this);
}
TaoWebSocket.prototype.onOpen=function()	{
	// if you want something to happen on connection
	// handle this state differently in your conn_handler
	window.console.debug('Connected'+this);
	this.conn_handler(CS_CONN,'Connected');
	this.retrys=0
	if(typeof(this.p_init)==='string')	{
		window.console.debug('sending...'+this.p_init);
		this.ws.send(this.p_init);
	}
}
TaoWebSocket.prototype.onError=function(error)	{
	//window.alert(this);
	if(this.ws.active)	{
		this.conn_handler(CS_ERR,error);
	}
}
TaoWebSocket.prototype.onMessage=function(event)	{
	//window.console.debug(event.data);
	if(this.ws.active)	{
		var evt=JSON.parse(event.data);
		//window.alert(evt);
		this.msg_handler(evt);
	}
}
TaoWebSocket.prototype.onClose=function(event)	{
		if(this.ws.active)	{
			this.conn_handler(CS_DIS,'Disconnected -'+event.code);
			this.ws.active=false;
		}
		window.console.debug('closed'+event.code+this.retrys);
		this.retrys++;
		if(this.retrys<timeouts.length)	{
			tmo=timeouts[this.retrys];
		} else	{
			tmo=10*60*1000
		}
		setTimeout(this.connect.bind(this),tmo)
}
TaoWebSocket.prototype.send=function(obj)	{
	window.console.debug('sending...'+obj);
	if(this.ws.active)	{
		if(typeof(obj)==='string')	{
			this.ws.send(obj);
		} else	{
			this.ws.send(JSON.stringify(obj));
		}
	} else	{
		window.console.debug('attempt to send on a close socket');
	}
}
TaoWebSocket.prototype.close=function(obj)	{
	this.ws.active=false;
	this.ws.close();
}

function connHandler(state,msg)	{
	window.console.debug('Connection State Change-'+state+' - '+msg);
	conn_state=document.getElementById('ws_conn_state');
	if(state==CS_INP)	{
		conn_state.innerHTML='Connecting to '+msg.split('://')[1].split('/')[0];
		conn_state.style.backgroundColor='#909090';
		conn_state.style.fontWeight='normal';
		conn_state.style.display='block';
	} else if(state==CS_CONN){
		conn_state.innerHTML='Connected';
		conn_state.style.backgroundColor='#349034';
		conn_state.style.fontWeight='normal';
		setTimeout(hideMessage,1000);
	} else if (state==CS_ERR)	{
		conn_state.innerHTML+='[ERROR]'+msg;
		conn_state.style.backgroundColor='#f44336';
		conn_state.style.fontWeight='bold';
		conn_state.style.display='block';
	} else if(state==CS_DIS)	{
		conn_state.innerHTML="Lost connection to webserver will try to reconnect...<span class='closebtn' onclick='tws.connect();'>Connect Now</span>";
		conn_state.style.backgroundColor='#f48034';
		conn_state.style.fontWeight='normal';
		conn_state.style.display='block';
	}
}

function hideMessage()	{
	conn_state.style.display='none';
}

function dataHandler(event)	{
	window.console.warn('Unhandled Event - '+JSON.stringify(event));
}

var tws
function setupData(uri,proto,init_packet,data_h,conn_h)	{
	window.console.debug('setupData - '+uri+','+proto+','+data_h);
	if (typeof(data_h)==='undefined') data_h=dataHandler;
	if (typeof(conn_h)==='undefined') conn_h=connHandler;
	tws=new TaoWebSocket(uri,proto,data_h,conn_h,init_packet);
	return tws
}

