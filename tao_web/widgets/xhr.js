function XhrRequest(successHandler,failHandler,progressHandler)	{
	//successHandler is called with mimetype / body of response
	//failhandler is called with string describing the failure
	//optional progressHandler is called with response length known and either fraction received or bytes received
	this.succesHandler=successHandler;
	this.failHandler=failHandler;
	this.busy=false;
	this.channel=new XMLHttpRequest();
	this.channel.responseType='json';
	window.console.debug(this.loadHandler);
	this.channel.addEventListener('load',this.loadHandler.bind(this));
	this.channel.addEventListener('error',this.errorHandler.bind(this));
	this.channel.addEventListener('abort',this.abortHandler.bind(this));
	if(progressHandler!=undefined)	{
		this.progressHandler=progressHandler;
		this.channel.addEventListener('progress',this.progressUpdate.bind(this))
	}
}
XhrRequest.prototype.loadHandler=function(evt)	{
	if(this.channel.status===200)	{
		this.successHandler(this.channel.getResponseHeader('content-type'),this.channel.response,this);
	}else	{
		this.failHandler('Server Returned Error, response code -'+String(this.channel.status),this);
	}
	this.busy=false;
}
XhrRequest.prototype.errorHandler=function(evt)	{
	this.failHandler('error sending request to server',this);
	this.busy=false;
}
XhrRequest.prototype.abortHandler=function(evt)	{
	this.failHandler('request aborted',this);
	this.busy=false;
}
XhrRequest.prototype.progressUpdate=function(evt)	{
	if(evt.lengthComputable)	{
		this.progressHandler(true,evt.loaded/evt.total);
	} else	{
		this.progressHandler(false,evt.loaded);
	}
}
XhrRequest.prototype.load=function(uri,verb,obj)	{
	if(!this.busy)	{
		this.busy=true;
		this.channel.open(verb,uri);
		if(obj!=undefined)	{
			this.channel.setRequestHeader("Content-Type","application/json");
			this.channel.send(JSON.stringify(obj));
		}
	} else	{	
		window.console.error("Attempt to use XhrRequest whilst busy.");
	}
}

