from tao_web.web_widget import View,XmlTemplateWidget
from tao_core.data_model import DataQuery,DataOp
from tao_core.tdf_core import dispatchWithCallback
from tao_web.widgets.tools import Toolbar,Button
from tao_web.widgets.box import VBox

class XmlBoundView(View):
	def __init__(self,data_src,main_templ,doc_uri=None,title='',widget_path=None):
		View.__init__(self,data_src,doc_uri=doc_uri,title=title)
		self.evt_map={}
		self.main_widget=VBox()
		if widget_path:
			self.data_widget=XmlTemplateWidget(templ=main_templ,widget_path=widget_path)
		else:
			self.data_widget=XmlTemplateWidget(templ=main_templ)
		self.main_widget.add(self.data_widget,size='flex')
		self.bar=Toolbar()
		self.bar.add(Button(title='Save',action='save_data'))
		self.main_widget.add(self.bar,size='30px')
		# required so that we pick up the resources
		self.add(self.main_widget)
	def dispatch(self,evt,cb=None):
		self.cb_map[evt]=cb
		# determine if this is a page request or changes from apply
		if evt['resp_type']=='application/json':
			body_data=json.loads(evt['req_body'])
			record_id=body_data.pop('record_id')
			if evt['req_type']=='PUT':
				# a partial update... check for conflict...
				data_evt=DataOp(rec=record_id,data=body_data)
			elif evt['req_type']=='DELETE':
				data_evt=DataOp(rec=record_id,delete=True)
			elif evt['req_type']=='POST':
				data_evt=DataOp(data=body_data)
			else:
				evt['resp_code']=405
				evt['resp_body']=json.dumps({'message':'unknown request type'})
				return
			self.evt_map[data_evt]=evt
			dispatchWithCallback(data_evt,self._dispatcher,self.dataResp)
		else:
			# treat everything else as a page request
			if len(evt['disp_path'])>1:
				# we have a record id in the path
				print 'Record Id -',evt['disp_path'][1]
				data_evt=DataOp(rec=evt['disp_path'][1])
				self.evt_map[data_evt]=evt
				dispatchWithCallback(self._dispatcher,data_evt,self.dataResp)
			else:
				evt['resp_code']=404
				return
	def dataResp(self,evt,exc=None):
		http_evt=self.evt_map.pop(evt)
		if exc and evt.name in ('org.digitao.DataUpdate','org.digitao.DataDel','org.digitao.DataNew'):
			# we should do more here to inform the client of the error
			http_evt['resp_body']=json.dumps({'message':repr(exc)})
		http_evt['result']=evt
		self.finish(http_evt,exc=exc)
	def render(self,data,doc_uri=None):
		if not doc_uri:
			doc_uri=self.doc_uri
		if data.name=='org.digitao.DataGet':
			body=self.main_widget.render(data['data'],doc_uri=doc_uri)
			return body
		else:
			# this is a Xhr request respond with json
			return json.dumps(evt['data'])


class XmlCollectionView(View):
	def __init__(self,data_src,col_templ,rec_templ,doc_uri=None,title=''):
		View.__init__(self,data_src,doc_uri=doc_uri,title=title)
		#FIX: needs new list view with row templ
		#self.main_widget=XmlTemplateWidget(templ=col_templ)
		self.record_widget=XmlTemplateWidget(templ=rec_templ)
	def clear_cache(self):
		# run through cache and drop anything more than 24 hours old
		pass
	def dataUpdate(self,evt,cb=None):
		#self.data_cache.append(
		pass
	def dispatch(self,evt,cb=None):
		# determine if this is a page request or changes from apply
		if evt['resp_type']=='application/json':
			body_data=json.loads(evt['req_body'])
			record_id=body_data.pop('record_id')
			if evt['req_type']=='PUT':
				# a partial update... check for conflict...
				data_evt=DataOp(rec=record_id,data=body_data)
			elif evt['req_type']=='DELETE':
				data_evt=DataOp(rec=record_id,delete=True)
			elif evt['req_type']=='POST':
				data_evt=DataOp(data=body_data)
			else:
				evt['resp_code']=405
				evt['resp_body']=json.dumps({'message':'unknown request type'})
				return
			self.evt_map[data_evt]=evt
			dispatchWithCallback(data_evt,self._dispatcher,self.dataResp)
		else:
			# treat everything else as a page request
			if len(evt['disp_path'])>1:
				# we have a record id in the path
				dispatchWithCallback(self._dispatcher,DataOp(rec=evt['disp_path'][1]),self.dataResp)
			else:
				dispatchWithCallback(self._dispatcher,DataQuery(evt['disp_path'][0]),self.dataResp)
	def dataResp(self,evt,exc=None):
		http_evt=self.evt_map.pop(evt)
		if exc and evt.name in ('org.digitao.DataUpdate','org.digitao.DataDel','org.digitao.DataNew'):
			# we should do more here to inform the client of the error
			http_evt['resp_body']=json.dumps({'message':repr(exc)})
		http_evt['result']=evt
		self.finish(http_evt,exc=exc)
	def render(self,data,doc_uri=None):
		if not doc_uri:
			doc_uri=self.doc_uri
		if isinstance(data,DataQuery):
			# this is the collection.... render with main_widget
			return self.main_widget.render(data['records'],doc_uri=doc_uri)
		else:
			# this is a record response
			if data.name=='org.digitao.DataGet':
				return self.record_widget.render(data['data'],doc_uri=doc_uri)
			else:
				# this is a Xhr request respond with json
				return json.dumps(evt['data'])

if __name__=="__main__":
	from tao_core import sigIO,sigIO_net
	from tao_web.http import HttpServerProtocol
	import os,time
	io_main=sigIO.getIO()
	io_main.start()
	def data_handler(evt,cb=None):
		if evt.name=='org.digitao.DataGet':
			evt['record_id']='test'
			for ind in range(1,4):
				evt['data']['test_'+str(ind)]='test_value_'+str(ind)
	view=XmlBoundView(data_handler,'../tests/xml_test.xml',widget_path=['./'])
	chan=io_main.newChannel(HttpServerProtocol,sigIO_net.TCPInetTransport,dispatcher=view)
	chan.listen((os.getenv('IP','0.0.0.0'),int(os.getenv('PORT',8080))))
	while 1:
		try:
			time.sleep(20)
		except KeyboardInterrupt:
			break
	chan.close()
	io_main.stop()
